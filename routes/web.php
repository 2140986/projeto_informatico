<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('auth.login');
});

Route::post('extract/ata/{id}', 'PointController@downloadAtaExtract');

Route::group(["middleware" => "auth"], function() {

    Route::get('meetings', 'MeetingController@index');
    Route::get('organisms', 'OrganismController@index');
    Route::get('create-meeting/{id}', 'MeetingController@createMeeting');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/meetings/{id}', ["as" => "meeting.show", "uses" => 'MeetingController@show']);
    Route::get('/organisms/{id}', ["as" => "organism.show", "uses" => 'OrganismController@show']);
    Route::get('api/v1/users-organism/{id}', 'MeetingController@getMemberMeeting');
    Route::post("/stores/meeting", ["as" => "meeting.store", "uses" => "MeetingController@storeMeeting"]);
    Route::post("/meetings/reject/{id}", "MeetingController@rejectMeeting");
    Route::post("/meetings/present/{id}", "MeetingController@validatePresence");
    Route::post("/meetings/positions/{id}", "MeetingController@editPositions");
    Route::post("/meetings/votes/{id}", "MeetingController@createVote");
    Route::post("/organisms/positions/{id}", "AdministrationController@savePositions");
    
    Route::get("/organisms/{organismId}/removeMember/{userId}", "AdministrationController@removeMember");
    
    Route::get('/meetings/{id}/in/{userId}', 'MeetingController@userIn');
    Route::get('/meetings/{id}/out/{userId}', 'MeetingController@userOut');
    Route::get('/meetings/{meetingId}/startVote/{voteId}', 'MeetingController@startVote');
    Route::get('/meetings/{meetingId}/endVote/{voteId}', 'MeetingController@endVote');
    Route::post('/meetings/{meetingId}/vote/{voteId}', 'MeetingController@vote');
    

    Route::get('notifications', "NotificationController@index");

    Route::get('administration', "AdministrationController@index")->name('administration');;
    Route::post('/administration/createOrganism', "AdministrationController@createOrganism");
    Route::post('/administration/addMember/{id}', "AdministrationController@addMemberToOrganism");
    Route::get('api/v1/auth/user', 'API\\UsersController@getAuthUser');
    

    Route::get('api/v1/users-organism/{id}', 'MeetingController@getMemberMeeting');

    Route::post("stores/meeting/{id}", ["as" => "meeting.store", "uses" => "MeetingController@storeMeeting"]);
    Route::post("attendance-list/{id}", ["as" => "meeting.attendance", "uses" => "MeetingUserController@getAttendanceList"]);
    Route::get('edit/meeting/{id}', 'MeetingController@editMeetingView');
    Route::post('update/meeting/{id}', 'MeetingController@updateMeeting');
    Route::post('change/status/{id}/{status}', 'MeetingController@changeStatusOfMeeting');

    Route::get('api/v1/template/{id}', 'API\\TemplateController@getTemplateContent');
    Route::post('point/{point_id}/template', 'PointTemplateController@addTemplateToPoint');
    Route::post('point/description/{id}', 'PointController@storeDescriptionToPoint');

    Route::get('api/v1/meeting/{id}', 'API\\MeetingController@getMeeting');
    Route::get('api/v1/meeting-points/{id}', 'API\\PointsController@getPointsOfMeeting');

    Route::post('meeting/main/{id}', 'MeetingController@storeIntroduction');
    Route::post('point/subpoint/{id}', 'SubpointsController@storeSubpoint');

    Route::post('main-ata/{id}', 'MeetingController@downloadAta');
    Route::post('extract/ata/{id}', 'MeetingController@downloadExtractAta');

    Route::post('storage/ata/{id}', 'UploadAtaController@storeAta');
    Route::post('download/ata', 'UploadAtaController@downloadAta');
    Route::get('api/v1/introduction/{id}','API\\MeetingController@getIntroductionMeeting');

    Route::get('api/v1/description-point/{id}','API\\PointsController@getPointsDescription');
    Route::get('api/v1/description-subpoint/{id}','API\\SubpointsController@getSubpointDescription');
    Route::get('api/v1/name-subpoint/{id}','API\\SubpointsController@getSubpointName');
    Route::post('update/subpoint/{id}', 'SubpointsController@updateSubpoint');

    Route::get('api/v1/conclusion/{id}', 'API\\MeetingController@getConclusionMeeting');
    Route::post('meeting/conclusion/{id}', 'MeetingController@storeConclusion');

    Route::post('store/template', 'TemplateController@storeTemplate');


});
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');


