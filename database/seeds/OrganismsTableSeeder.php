<?php

use Illuminate\Database\Seeder;

class OrganismsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Organism::class, 50)->create();
    }
}
