<?php

use Illuminate\Database\Seeder;

class VotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Vote::class, 50)->create()->each(function ($u) {
            $u->votes()->save(factory(App\Option::class)->make());
            $u->votes()->save(factory(App\Option::class)->make());
        });
    }
}
