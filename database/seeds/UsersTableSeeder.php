<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(App\User::class, 50)->create();

        //4 Users:
        //1 Admin
        //1 Presidente
        //1 Secretario
        //1 Membro
        DB::table('users')->insert([
            'name' => "Administrator",
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'admin' => 1, 
            'blocked' => 0,
            'phone' => 911111111
        ]);
        DB::table('users')->insert([
            'name' => "President",
            'email' => 'president@gmail.com',
            'password' => bcrypt('secret'),
            'admin' => 0, 
            'blocked' => 0,
            'phone' => 911111111
        ]);
        DB::table('users')->insert([
            'name' => "Secretary",
            'email' => 'secretary@gmail.com',
            'password' => bcrypt('secret'),
            'admin' => 0, 
            'blocked' => 0,
            'phone' => 911111111
        ]);
        DB::table('users')->insert([
            'name' => "Member",
            'email' => 'member@gmail.com',
            'password' => bcrypt('secret'),
            'admin' => 0, 
            'blocked' => 0,
            'phone' => 911111111
        ]);
        
        //Criaçao de um orgao
        DB::table('organisms')->insert([
            'name' => "Eng. Informática",
            'lastMinutePageNumber' => 1,
        ]);

        //Associação dos users ao orgao
        DB::table('organism_user')->insert([
            'user_id' => 2,
            'organism_id' => 1,
            'position' => 1,
        ]);
        DB::table('organism_user')->insert([
            'user_id' => 3,
            'organism_id' => 1,
            'position' => 2,
        ]);
        DB::table('organism_user')->insert([
            'user_id' => 4,
            'organism_id' => 1,
            'position' => 0,
        ]);

        //Criação da meeting
        DB::table('meetings')->insert([
            'seqNumber' => 1,
            'date' => date("Y-m-d"),
            'title' => "Reunião de aprovação de estatutos",
            'location' => "Sala 0",
            'initHour' => DateTime::createFromFormat('H\h i\m s\s','18h 00m 00s'),
            'duration' => "2",
            'state' => 0,
            'type' => 0,
            'introduction' => "Aqui será a introdução",
            'conclusion' => "Aqui será a conclusão",
            'attachments' => 0,
            'minute' => "ata1.pdf",
            'organism_id' => 1,
        ]);

        DB::table('meetings')->insert([
            'seqNumber' => 2,
            'date' => date("Y-m-d"),
            'title' => "Reunião de teste",
            'location' => "Sala 3",
            'initHour' => DateTime::createFromFormat('H\h i\m s\s','16h 00m 00s'),
            'duration' => "2",
            'state' => 0,
            'type' => 1,
            'introduction' => "Aqui será a introdução",
            'conclusion' => "Aqui será a conclusão",
            'attachments' => 0,
            'minute' => "ata1.pdf",
            'organism_id' => 1,
        ]);
        //Criação da convocatoria
        DB::table('notices')->insert([
            'description' => "Esta convocado para a reuniao 1 no dia x bla bla bla",
            'sendDate' => date("Y-m-d"),
            'meeting_id' => 1
        ]);

        DB::table('notices')->insert([
            'description' => "Esta convocado para a reuniao 2 no dia x bla bla bla",
            'sendDate' => date("Y-m-d"),
            'meeting_id' => 2
        ]);

        //Associação da meeting aos users
        DB::table('meeting_user')->insert([
            'meeting_id' => 1,
            'user_id' => 2,
            'rejected' => false,
            'role' => 1
        ]);

        DB::table('meeting_user')->insert([
            'meeting_id' => 1,
            'user_id' => 3,
            'rejected' => false,
            'role' => 2
        ]);

        DB::table('meeting_user')->insert([
            'meeting_id' => 1,
            'user_id' => 4,
            'rejected' => false,
            'role' => 0
        ]);

        DB::table('meeting_user')->insert([
            'meeting_id' => 2,
            'user_id' => 2,
            'rejected' => false,
            'role' => 1
        ]);

        DB::table('meeting_user')->insert([
            'meeting_id' => 2,
            'user_id' => 3,
            'rejected' => false,
            'role' => 2
        ]);

        DB::table('meeting_user')->insert([
            'meeting_id' => 2,
            'user_id' => 4,
            'rejected' => false,
            'role' => 0
        ]);

        DB::table('templates')->insert([
            'title' => 'Agredecimento de presenças (ínicio)',
            'content' => "<p style=\"font-weight: bold;\">
                                ----------- O coordenador começou por agradecer a presença de todos e a disponibilidade do(a) colega
                                ____________
                                em secretariar a reunião,
                                e informou que foram dispensados da reunião os colegas com outras reuniões e/ou compromissos agendados,
                                e ainda os colegas que legalmente se encontram ausentes. ------------------------------------------------------------------
                            </p>",
        ]);

        DB::table('templates')->insert([
            'title' => 'Distribuição de serviço docente',
            'content' => 'O coordenador informou que a última versão (v20) de distribuição de serviço docente (DSD) para 2015/16 foi enviada por mensagem a todos os colegas. Os documentos encontram-se em anexo à ata.'
        ]);


    }
}
