<?php

use Illuminate\Database\Seeder;

class MeetingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Meeting::class, 50)->create()->each(function ($u) {
            $u->votes()->saveMany(factory(App\Vote::class, 2)->create()->each(function ($t) {
                $t->options()->saveMany(factory(App\Option::class, 2)->create());
            }));
            $u->points()->saveMany(factory(App\Point::class, 3)->create());
            $u->notices()->saveMany(factory(App\Notice::class, 2)->create());
        });
    }
}
