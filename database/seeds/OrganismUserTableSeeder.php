<?php

use Illuminate\Database\Seeder;

class OrganismUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\OrganismUser::class, 50)->create();
    }
}
