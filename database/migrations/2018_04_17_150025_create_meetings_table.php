<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seqNumber');
            $table->dateTime('initDate');
            $table->dateTime('endDate');
            $table->integer('state');
            $table->integer('type');
            $table->longText('introduction');
            $table->longText('conclusion');
            $table->text('attachments');
            $table->text('minute');
            $table->text('presenceCode')->nullable();
            $table->integer('organism_id')->unsigned()->nullable();
            $table->foreign('organism_id')->references('id')
                ->on('organisms');//->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}
