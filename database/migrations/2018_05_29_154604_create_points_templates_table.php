<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points_templates', function (Blueprint $table) {
            $table->integer('point_id')->unsigned()->nullable();
            $table->foreign('point_id')->references('id')
                ->on('points');//->onDelete('cascade');
            $table->integer('template_id')->unsigned()->nullable();
            $table->foreign('template_id')->references('id')
                ->on('templates');//->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points_templates');
    }
}
