<?php

use Faker\Generator as Faker;

$factory->define(\App\OrganismUser::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween($min = 1, $max = 50),
        'organism_id' => $faker->numberBetween($min = 1, $max = 50),
        'position' => $faker->numberBetween($min = 0, $max = 3),
    ];
});
