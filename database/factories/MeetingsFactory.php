<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Meeting::class, function (Faker $faker) {
    return [
        'seqNumber' => $faker->numberBetween($min = 1, $max = 100),
        'initDate' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+ 30 days'),
        'endDate' => $faker->dateTimeBetween($startDate = '+ 30 days', $endDate = '+ 31 days'),
        'state' => 0,
        'type' => $faker->numberBetween($min = 0, $max = 1),
        'introduction' => $faker->text($maxNbChars = 200),
        'conclusion' => $faker->text($maxNbChars = 200),
        'attachments' => "ficheiro." . $faker->fileExtension,
        'minute' => "ata.pdf",
        'organism_id' => $faker->numberBetween($min = 1, $max = 50),
    ];
});
