<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Organism::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'seqNumber' => $faker->numberBetween($min = 1, $max = 100),
        'lastMinutePageNumber' => $faker->numberBetween($min = 100, $max = 2000),
    ];
});
