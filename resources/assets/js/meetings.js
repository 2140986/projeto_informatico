import Vue from 'vue';
import axios from 'axios';
import Editor from '@tinymce/tinymce-vue';


Vue.use(require('vue-moment'));
// register modal component
Vue.component('modal', {
    template: '#modal-template'
});

new Vue({
    el: '#organisms',

    data: {
        member: '',
        memberName: '',
        users: [],
        description_r: '',
        hour: '',
        day: '',
        classroom: '',
        input: '',
        orders: [],
        rows: [{}],
        index: 100,
        endHour: '',
        documentation: '',
        user: {},
        membersCheck:[{}],
        file: '',
    },
    components: {
        'editor': Editor,
    },
    methods: {
        getUsersByOrganism() {
            axios.get('api/v1/users-organism/' + this.member , {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                // JSON responses are automatically parsed.
                this.users = response.data;
            })
        },

        getAuthUser() {
            axios.get(this.member +'/api/v1/auth/user', {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                    // JSON responses are automatically parsed.
                    this.user = response.data;
                })
        },

        addRow: function (index) {
            try {
                this.rows.splice(index + 1, 0, {});
            } catch (e) {
                console.log(e);
            }
        },
        removeRow: function (index) {
            this.rows.splice(index, 1);
        },

        getInputName(index, dataName) {
            return "rows[" + index + "][" + dataName + "]";
        },

        getMembersName(index, dataName) {
            return "membersCheck[" + index + "][" + dataName + "]";
        },




    },
    computed: {
        emailText() {
            let r = this.rows.map(function(row) {
               return '<li style="height: 20px;">'+row.number+'</li>'
            }).join("");

            let doc = '';
            if(this.documentation !== '' || this.file !== '') {
                doc = "<p>A documentação encontra-se presente no link:" + this.documentation +".</p>"
            }

           return this.input = "<p>Caros colegas,</p><p>Ao abrigo do artº 44º dos Estatutos da Escola Superior de Tecnologia e Gestão de Instituto Politécnico de Leiria, venho por este meio convocar uma reunião do Plenário do Departamento de "+ this.memberName +" para as " + this.hour +" do dia "+ this.day + ", no " + this.classroom + ", " +
               "com a seguinte ordem de trabalhos:</p><ol>"+r+"</ol>"
               + "<p>A reunião tem uma duração máxima prevista de "+ this.endHour +".</p>"
               + doc +"<p>Cumprimentos, </p>"
               + "<p>"+this.user.name+"</p>" +
               "<p>"+ this.description_r +"<br>Escola Superior de Tecnologia e Gestão" +
               "<br>Instituto Politécnico de Leiria</p>";
        },
    },

    mounted() {
        this.getAuthUser();
        console.log(this.rows);
    }
});

