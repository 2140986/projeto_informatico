import Vue from 'vue';
import axios from 'axios';
import Editor from '@tinymce/tinymce-vue';


Vue.use(require('vue-moment'));

new Vue({
    el: '#show-m',
    data: function() {
        return {
            meeting: {},
            template: '',
            introduction: '',
            form: {
                parent_id: [],
            },
            sub: {
                subpoint: [],
                name: []
            },
            subpoints: [],

            main: {
                parent: '',
            },
            points: [],

            pointTiny: [{}],
            subpointTiny: [{}],

            conclusion: '<div style="text-align: center;"><p>(Sua Função)</p><p>__________________________________________________</p><br>' +
            '<p>Secretário(a)</p><p>__________________________________________________</p></div>',
            conclusiondb: '',

        }
    },
    components: {
        'editor': Editor,
    },
    methods: {
        getTemplateContent(index) {
           axios.get('/api/v1/template/'+ this.template, {
                headers: { 'Content-Type': 'application/json'}})
            .then(response => {
                this.form.parent_id[index] += response.data;
                this.$forceUpdate(this.form.parent_id[index] );

            })
        },

        getDescriptionFromPoint(index, point) {
            axios.get('/api/v1/description-point/'+point, {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                    this.form.parent_id[index] = response.data;
                    this.$forceUpdate(this.form.parent_id[index]);
                });
        },

        getDescriptionFromSubpoint(index, subpoint) {
            axios.get('/api/v1/description-subpoint/'+subpoint, {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                    this.sub.subpoint[index] = response.data;
                    this.$forceUpdate(this.sub.subpoint[index]);
                });
        },
        getNameFromSubpoint(index, subpoint) {
            axios.get('/api/v1/name-subpoint/'+subpoint, {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                    this.sub.name[index] = response.data;
                    this.$forceUpdate(this.sub.name[index]);
                });
        },

        getTemplateContentToMain() {
            axios.get('/api/v1/template/'+ this.template, {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                    this.main.parent += response.data;
                    this.$forceUpdate(this.main.parent);

                })
        },

        getTemplateContentToConclusion() {
            axios.get('/api/v1/template/'+ this.template, {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                    this.conclusion += response.data;
                    this.$forceUpdate(this.conclusion);

                })
        },

        getConclusion(value) {
            if(this.conclusiondb) {
                this.conclusion = this.conclusiondb;
            }

        },


        getPoints(value) {
            axios.get('/api/v1/meeting-points/'+value, {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                    this.points = response.data;
                    let r = this.points.map(function(row) {
                        return '<p style="font-weight: bold;">Ponto '+row.number+'. '+row.name+'</p>'
                    }).join("");

                    if(!this.introduction) {
                        this.main.parent = '<p>Ao ________  reuniu, às ________ no(a) '+this.$refs.location.value+ ' do Campus ___ do IPL, o Plenário do Departamento de '+this.$refs.organism.value+ ', estando presentes e ausentes os docentes identificados na lista de presenças anexa.</p>' +
                            '<p>O coordenador aceitou as justificações apresentadas por todos os colegas ausentes.</p>' +
                            '<p>A ordem de trabalhos contemplava os seguintes pontos:</p>'+r+
                            '<p>Verificada a existência de quórum, o Coordenador de Departamento, que preside ao Plenário, deu início à ordem de trabalhos.</p>';
                    } else {
                        this.main.parent = this.introduction;

                    }


                })
        },


        getPointTinyName(index, dataName) {
            return "pointTiny[" + index + "][" + dataName + "]";
        },

        getSubpointTinyName(index, dataName) {
            return "subpointTiny[" + index + "][" + dataName + "]";
        },

        getMeeting(value) {
            axios.get('/api/v1/meeting/'+value, {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                    this.meeting = response.data;
                })
        },

        addRow: function(index) {
            this.subpoints.push({
                    index: index,
                    name: '',
                    label: '',
            });
        },

        getTemplateContentSubpoint(subpoint) {
            axios.get('/api/v1/template/'+ this.template, {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                    subpoint.label += response.data;
                    this.$forceUpdate(subpoint.label);

                })
        },

        getTemplateSubpoint(index) {
            axios.get('/api/v1/template/'+ this.template, {
                headers: { 'Content-Type': 'application/json'}})
                .then(response => {
                    this.sub.subpoint[index] += response.data;
                    this.$forceUpdate(this.sub.subpoint[index]);

                })
        }


    },
    computed: {

    },

    mounted() {
        axios.get('/api/v1/introduction/'+this.$refs.meeting.value, {
            headers: { 'Content-Type': 'application/json'}})
            .then(response => {
                this.introduction = response.data;
                this.getPoints(this.$refs.meeting.value);
            });
        axios.get('/api/v1/conclusion/'+this.$refs.meeting.value, {
            headers: { 'Content-Type': 'application/json'}})
            .then(response => {
                this.conclusiondb = response.data;
                this.getConclusion(this.$refs.meeting.value);
            });
        this.getMeeting(this.$refs.meeting.value);
        //this.getIntroductionFromMeeting();
        //this.mainText();
    }

});
