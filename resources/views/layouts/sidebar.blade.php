@extends('layouts.navbar')

@section('leftbar')
<div class="sidebar">
    <ul class="nav">
        <li style="padding: 10px 0 0;">
            <a href="/meetings"><i class="fa fa-calendar fa-fw"></i><span>Meu Calendário</span></a>
        </li>
        <li>
            <a href="/notifications"><i class="fa fa-sticky-note fa-fw"></i><span>Notificações</span></a>
        </li>
        <li>
            <a href="/organisms"><i class="fa fa-suitcase fa-fw"></i><span>Meus Orgãos</span></a>
        </li>
        <li>
    </ul>
</div>
<div class="content">
    @yield('content')
</div>
@yield('sidebar')
@endsection
