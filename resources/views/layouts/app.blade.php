<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name') }}</title>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Styles -->
    @yield('style')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <!------ Include the above in your HEAD tag ---------->
    
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=u1p3byktyqznf8bi229hakncdpkxe65vm0aieikszs3d6wk1"></script>
</head>
<body>
    <div id="app">
            @if(!Auth::guest())
            <div class="nav-side-menu">
                <div class="brand">
                    <a href="/home" style="text-decoration: none; cursor: pointer; color:white;">ElabAtas</a>
                </div>
                <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
                
                <div class="menu-list">
                    
                    <ul id="menu-content" class="menu-content collapse out">
                        {{-- <li>
                            <input type="text" placeholder="Pesquisar..." class="form-control" style="width: 70%; display: inline-block;"/>
                            <a style="width: 20%; display: inline-block; text-align: right" href=""><i class="fa fa-search"></i></a>
                        </li>
                        <br/> --}}
                        
                        <li>
                            <a href="/home" title="Ver Calendário com reuniões.">
                                <i class="fa fa-calendar fa-lg"></i> Meu Calendário
                            </a>
                        </li>
                        <li title="Reuniões para as quais está convocado.">
                            <a href="/notifications">
                                <i class="fa fa-sticky-note fa-lg"></i> Convocatórias
                            </a>
                        </li>
                        <li title="Permite ver os orgãos a que pertence como criar reuniões se tiver privilégio para tal.">
                            <a href="/organisms">
                                <i class="fa fa-suitcase fa-lg"></i> Meus orgãos
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </div>
            @endif
            <div class="content">
                <div class="top-nav">
                    <!-- Right Side Of Navbar -->
                    <div>
                        
                        <ul class="nav navbar-nav navbar-right topNav">
                            <!-- Authentication Links -->
                            @guest
                            <li>
                            <div class="form-group">
                                <br><br><br><br><br>
                                <p style="font-size: 15px;">A ElabAtas é a aplicação que o ajuda na gestão das suas reuniões.
                                    Ferramenta perfeita para a otimização dos processos necessários para a realização de reuniões. </p>
                            </div>
                            </li>
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Registo</a></li>
                            @else
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre> Bem vindo(a)
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                
                                <ul class="dropdown-menu">
                                    @if(Auth::user()->isAdmin())
                                    <li><a href="{{ route('administration') }}">Administração</a></li>
                                    @endif
                                    <li>
                                        <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                    </ul>
                </div>
                
            </div>
            <div class="ncontent">
                @yield('content')
            </div>
        </div>
        
        <style>
            .ncontent {
                width: 90%;
                margin: auto;
                overflow-y: auto;
            }
            .topNav {
                padding-right: 10px; 
                margin-top: 0;
            }
            .top-nav {
                background-color: #2e353d;
                height: 40px;
                text-align: center;
            }
            
            .dropdown-menu {
                text-align: center;
                background-color: #3f4144 !important;
                z-index: 1000 !important;
            }
            .open {
                z-index: 10000 !important;
            }
            
            .content {
                
                width: 100%;
                float: none;
            }
            .nav-side-menu {
                overflow: auto;
                font-family: verdana;
                font-size: 12px;
                font-weight: 200;
                background-color: #2e353d;
                position: fixed;
                top: 0px;
                width: 25%;
                height: 100%;
                color: #e1ffff;
            }
            .nav-side-menu .brand {
                background-color: #23282e;
                line-height: 50px;
                display: block;
                text-align: center;
                font-size: 25px;
            }
            .nav-side-menu .toggle-btn {
                display: none;
            }
            .nav-side-menu ul,
            .nav-side-menu li {
                list-style: none;
                padding: 0px;
                margin: 0px;
                line-height: 35px;
                cursor: pointer;
                /*    
                .collapsed{
                    .arrow:before{
                        font-family: FontAwesome;
                        content: "\f053";
                        display: inline-block;
                        padding-left:10px;
                        padding-right: 10px;
                        vertical-align: middle;
                        float:right;
                    }
                }
                */
            }
            .nav-side-menu ul :not(collapsed) .arrow:before,
            .nav-side-menu li :not(collapsed) .arrow:before {
                font-family: FontAwesome;
                content: "\f078";
                display: inline-block;
                padding-left: 10px;
                padding-right: 10px;
                vertical-align: middle;
                float: right;
            }
            .nav-side-menu ul .active,
            .nav-side-menu li .active {
                border-left: 3px solid #d19b3d;
                background-color: #4f5b69;
            }
            .nav-side-menu ul .sub-menu li.active,
            .nav-side-menu li .sub-menu li.active {
                color: #d19b3d;
            }
            .nav-side-menu ul .sub-menu li.active a,
            .nav-side-menu li .sub-menu li.active a {
                color: #d19b3d;
            }
            .nav-side-menu ul .sub-menu li,
            .nav-side-menu li .sub-menu li {
                background-color: #181c20;
                border: none;
                line-height: 28px;
                border-bottom: 1px solid #23282e;
                margin-left: 0px;
            }
            .nav-side-menu ul .sub-menu li:hover,
            .nav-side-menu li .sub-menu li:hover {
                background-color: #020203;
            }
            .nav-side-menu ul .sub-menu li:before,
            .nav-side-menu li .sub-menu li:before {
                font-family: FontAwesome;
                content: "\f105";
                display: inline-block;
                padding-left: 10px;
                padding-right: 10px;
                vertical-align: middle;
            }
            .nav-side-menu li {
                padding-left: 0px;
                border-left: 3px solid #2e353d;
                border-bottom: 1px solid #23282e;
            }
            .nav-side-menu li a {
                text-decoration: none;
                color: #e1ffff;
                font-size: 15px;
            }
            .nav-side-menu li a i {
                padding-left: 10px;
                width: 20px;
                padding-right: 20px;
            }
            .nav-side-menu li:hover {
                border-left: 3px solid #d19b3d;
                background-color: #4f5b69;
                -webkit-transition: all 1s ease;
                -moz-transition: all 1s ease;
                -o-transition: all 1s ease;
                -ms-transition: all 1s ease;
                transition: all 1s ease;
            }
            @media (max-width: 767px) {
                .nav-side-menu {
                    position: relative;
                    width: 100%;
                }
                .nav-side-menu .toggle-btn {
                    display: block;
                    cursor: pointer;
                    position: absolute;
                    right: 10px;
                    top: 10px;
                    z-index: 10 !important;
                    padding: 3px;
                    background-color: #ffffff;
                    color: #000;
                    width: 40px;
                    text-align: center;
                }
                .brand {
                    text-align: left !important;
                    font-size: 15px;
                    padding-left: 20px;
                    line-height: 50px !important;
                    
                }
            }
            
            @media (min-width: 767px) {
                .nav-side-menu .menu-list .menu-content {
                    display: block;
                }
                
                @if(!Auth::guest())
                .content {
                    width: 75%;
                    float: right;
                }
                @endif
                
                .top-nav {
                    text-align: none;
                    height: 50px;
                }
                
                .dropdown-menu {
                    text-align: inherit;
                    background-color: inherit !important;
                }
            }
            
            body {
                margin: 0px;
                padding: 0px;
            }
            
            .collapse {
                visibility: initial !important;
            }
            
            
        </style>
    </div>
    @yield('scripts')
    <!-- Scripts -->
    
    <script src="../bootstrap/js/modal.js"></script>
    
</body>
</html>
