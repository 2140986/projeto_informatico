@extends('layouts.app')

@section('content')

<div>
    <h1 class="center">Meus Orgãos</h1>
    <hr>
    <p class="center">Nesta lista poderá encontrar todos os orgãos a que pertence.</p>
    <hr>
    <div class="table">
        <table>
            <tr>
                <th>Orgão</th>
                <th>Posição</th>
                <th>Ver</th>
                <th>Ações</th>
            </tr>
            @foreach($organisms as $key => $organism)
            <tr>
                <td>{{$organism->name}}</td>
                <td>{{$user->positionToStr($organism->id)}}</td>
                <td><a href="/organisms/{{$organism->id}}" class="btn btn-info" role="button">Ver orgão</a></td>
                <td>
                    @if($user->organisms->find($organism->id)->pivot->position == 1 || $user->organisms->find($organism->id)->pivot->position == 2)
                        <a href="/create-meeting/{{$organism->id}}" class="btn btn-info" role="button">Criar Reunião</a>
                    @else
                        <span>Não possui privilégios para convocar uma reunião.</span>
                    @endif
                </td>
            </tr>
            @endforeach
        </table>
    </div>
    
</div>
<style>
    .table {
        overflow-y: auto;
    }
    .center {
        text-align: center;
    }
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    
    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    
    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
@endsection

