@extends('layouts.app')

@section('content')
<br>
<h1 class="center">{{$meeting->title}}</h1>
<h3 class="center subtitle">{{$user->meetingRoleToStr($meeting->id)}}</h3>
<hr>
<p class="center">Aqui poderá encontrar toda a informação sobre a reunião.</p>
<hr>
{{-- MEETING REJEITADA --}}
@if($pivot->rejected == 1)
<div class="alert alert-danger middle center" style="width: 60%;" role="alert">
    <h4>Informou que não ia estar presente nesta reunião.</h4>
    <p><b>A sua justificação:</b> {{$pivot->justification}} </p>
    <p>
        @if($pivot->justificationAccepted == 1)
        <b>Estado:</b> Justificação aprovada
        @else
        <b>Estado:</b> Aguarda aprovação
        @endif
    </p>
    {{-- Mostrar se foi ou não aprovada --}}
</div>
@endif

<h3 class="center">Informações</h3>
<div class="center">
    <table class="infoTable">
        <tr>
            <th>Data</th>
            <td>{{$meeting->date->format('d/m/Y')}} - {{$meeting->initHour}} ({{$meeting->duration}} horas)</td>
        </tr>
        <tr>
            <th>Tipo</th>
            <td>{{$meeting->typeToStr()}}</td>
        </tr>
        <tr>
            <th>Localização</th>
            <td>{{$meeting->location}}</td>
        </tr>
        <tr>
            <th>Estado</th>
            <td>{{$meeting->stateToStr()}}</td>
        </tr>
        <tr>
            <th>Ordem de trabalhos</th>
            <td>
                @foreach ($meeting->points as $key => $point)
                <p><b>Ponto {{$key + 1}}:</b> {{$point->name}}</p>
                @endforeach
            </td>
        </tr>
        <tr>
            <th>Presidente</th>
            <td>@if($meeting->president()) {{$meeting->president()->name}} @else Não há presidente definido @endif</td>
        </tr>
        <tr>
            <th>Secretário</th>
            <td>@if($meeting->secretary()) {{$meeting->secretary()->name}} @else Não há secretário definido @endif</td>
        </tr>
        <tr>
            <th>Ações</th>
            <td>
                @if($user->meetings->find($meeting->id)->pivot->role == 1 && $meeting->state == 0)
                    <div class="center rejectDiv">
                        <form method="GET" action="/edit/meeting/{{$meeting->id}}" enctype="application/x-www-form-urlencoded" >
                            {{ csrf_field() }}
                            <button class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i>   Editar reunião</button>
                        </form>
                    </div>
                    <form method="POST" action="/change/status/{{$meeting->id}}/1" enctype="application/x-www-form-urlencoded" >
                        {{ csrf_field() }}
                        <div class="center rejectDiv">
                            <button type="submit" class="btn btn-warning" id="open-modal" data-toggle="modal" data-target="#myModal"><i class="fa fa-play"></i>  Começar reunião</button>
                        </div>
                    </form>
                @endif
                @if($user->meetings->find($meeting->id)->pivot->role == 1 && $meeting->state == 1)
                    <form method="POST" action="/change/status/{{$meeting->id}}/2" enctype="application/x-www-form-urlencoded" >
                        {{ csrf_field() }}
                        <div class="center rejectDiv">
                            <button type="submit" class="btn btn-warning" id="open-modal" data-toggle="modal" data-target="#myModal"><i class="fa fa-stop"></i>  Finalizar reunião</button>
                        </div>
                    </form>
                @endif
                @if($user->meetings->find($meeting->id)->pivot->role == 0)
                    <span>Não possui privilégios para aceder às ações.</span>
                @endif
            </td>
        </tr>
    </table>
</div>
</br>
{{-- @if($meeting->date->format('d/m/Y') == Carbon\Carbon::now()->format('d/m/Y') && (Carbon\Carbon::parse($meeting->initDate)->format('H:i:s') <=(Carbon\Carbon::now()->setTimezone('Europe/London')->format('H:i:s')))) --}}
<br>
{{-- @endif --}}
<div class="row">
    @if($user->meetings->find($meeting->id)->pivot->role == 1 )
        <div class="col-6 col-md-4">
            <form method="POST" action="/attendance-list/{{$meeting->id}}" enctype="application/x-www-form-urlencoded" >
                {{ csrf_field() }}
                <button class="btn btn-info"><i class="glyphicon glyphicon-download-alt"></i>    Download da lista de presenças</button>
            </form>
        </div>
    @endif
    @if(($user->meetings->find($meeting->id)->pivot->role == 1 || $user->meetings->find($meeting->id)->pivot->role == 2) && ($meeting->state == 1 || $meeting->state == 2))
        <div class="col-6 col-md-4">
            <form method="POST" action="/main-ata/{{$meeting->id}}" enctype="application/x-www-form-urlencoded" >
                {{ csrf_field() }}
                <button class="btn btn-success"><i class="glyphicon glyphicon-download-alt"></i>Ver ata em formato pdf</button>
            </form>
        </div>
        <div class="col-4 col-md-3">
            <button type="button" class="btn btn-success" id="open-modal" data-toggle="modal" data-target="#extract"><i class="glyphicon glyphicon-download-alt"></i>   Extracto da Ata</button>
        </div>
        <div class="modal fade" id="extract" role="dialog" style="padding-top: 250px;" >
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Extrato de um ponto ou subponto</h4>
                    </div>
                    <form method="POST" action="/extract/ata/{{$meeting->id}}" enctype="application/x-www-form-urlencoded" >
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="meetingStatus">Selecione o ponto ou subponto:</label>
                                <select id="meetingStatus" name="extract_ata" class="form-control col-sm-3">
                                    <option value="" disabled selected>Selecione um ponto</option>
                                    @foreach($meeting->points as $index => $point)
                                        <option value="{{$point->id}}">{{$point->name}}</option>
                                        @foreach($point->subpoints as $index => $subpoint)
                                            <option value="{{$point->id}}-{{$subpoint->id}}">{{$subpoint->name}}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Confirmar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
</div>
{{-- REJEITAR MEETING --}}
@if($pivot->rejected == 0 && $meeting->state == 0 && $pivot->role != 1 && $pivot->role != 2)
<div class="center rejectDiv">
    <label>Não poderei estar presente:</label>
    <input type="checkbox" name="rejectMeetingCheck" class="rejectMeetingCheck">
    <div class="rejectMeetingDiv">
        <form method="POST" action="/meetings/reject/{{$meeting->id}}" enctype="application/x-www-form-urlencoded" >
            {{ csrf_field() }}
            <p>Justificação: <input type="text" name="justification" required></p>
            <button class="btn btn-danger" type="submit">Enviar</button>
        </form>
    </div>
</div>
@endif

{{-- DEFINIR PAPEIS NA MEETING --}}
@if($user->meetings->find($meeting->id)->pivot->role == 1 && $meeting->state == 0)
<hr>
<div class="alert alert-warning">
    <h1 style="text-align: center"><b>Painel de administração</b></h1>
    <h3><b>Definir papéis:</b></h3>
    <form method="POST" action="/meetings/positions/{{$meeting->id}}" enctype="application/x-www-form-urlencoded" >
        {{ csrf_field() }}
        <b>Secretário:</b>
        <select class="form-control form-control-line" name="secretary">
            @foreach ($meeting->users as $key => $userC)
            @if($userC->meetings->find($meeting->id)->pivot->role != 1) 
            @if($userC->meetings->find($meeting->id)->pivot->role == 2)
            <option id="meeting_member" selected value="{{$userC->id}}">{{$userC->name}} - Actual</option>
            @else
            <option id="meeting_member" value="{{$userC->id}}">{{$userC->name}}</option>
            @endif
            @endif
            @endforeach
        </select>
        <br>Definir outro presidente para esta reunião:
        <input type="checkbox" name="definePresidentCheck" class="definePresidentCheck">
        <div class="definePresident">
            <select class="form-control form-control-line" name="president">
                @foreach ($meeting->users as $key => $userC)
                @if($userC->meetings->find($meeting->id)->pivot->role != 1) 
                <option id="meeting_member" value="{{$userC->id}}">{{$userC->name}}</option>
                @endif
                @endforeach
            </select>
        </div>
        <div class="center btnSave">
            <button class="btn btn-info" type="submit">Guardar</button>
        </div>
    </form>
    <hr/>
    <h3><b>Não poderão estar presentes:</b></h3>
    <table style="width: 100%">
        <tr>
            <th>Nome:</th>
            <th>Justificação:</th>
            <th>Aceitar</th>
        </tr>
        
        @foreach ($meeting->users as $key => $userC)
        @if($userC->meetings->find($meeting->id)->pivot->rejected == 1) 
        <tr>
            <td>{{$userC->name}}</td>
            <td>{{$userC->meetings->find($meeting->id)->pivot->justification}}</td>
            <td><button class="btn btn-info">Aceitar</button></td>
        </tr>
        @endif
        @endforeach
    </table>
</div>
@endif
{{-- REUNIÃO A DECORRER --}}
@if($meeting->state == 1 && $pivot->justificationAccepted != 1)
{{-- ADMIN -> Codigo marcar presenca --}}
@if($pivot->role == 1)
<div class="alert alert-warning">
    <h1 style="text-align: center"><b>Painel de administração</b></h1>
    <hr/>
    <h2 style="text-align: center">Gestão de presenças:</h2>
    <h3 style="text-align: center">Código para marcar presença: <i>{{$meeting->presenceCode}}</i></h3>
    <div>
        <div class="presences">
            <b>Presenças confirmadas:</b>
            <ul>
                @foreach ($meeting->users as $key => $userC)
                @if($userC->meetings->find($meeting->id)->pivot->present == 1) 
                <li>{{$userC->name}} - {{$userC->meetingRoleToStr($meeting->id)}}</li>
                @endif
                @endforeach
            </ul>
            <b>Presenças não confirmadas:</b>
            <ul>
                @foreach ($meeting->users as $key => $userC)
                @if($userC->meetings->find($meeting->id)->pivot->present != 1) 
                <li>{{$userC->name}} - {{$userC->meetingRoleToStr($meeting->id)}}</li>
                @endif
                @endforeach
            </ul>
        </div>
        <div class="presencesNow">
            <b>Presentes agora: (Membros que vão ter acesso as votações)</b>
            <ul> 
                @foreach ($meeting->users as $key => $userC)
                @if($userC->meetings->find($meeting->id)->pivot->present == 1) 
                @if($userC->meetings->find($meeting->id)->pivot->presentNow == 1)
                <li>
                    {{$userC->name}} - {{$userC->meetingRoleToStr($meeting->id)}}
                    <a href="{{ url('/meetings/' . $meeting->id . '/out/' . $userC->id) }}" class="btn btn-warning">Saiu</a>
                </li>
                @else
                <li>
                    {{$userC->name}} - {{$userC->meetingRoleToStr($meeting->id)}}
                    <a href="{{ url('/meetings/' . $meeting->id . '/in/' . $userC->id) }}" class="btn btn-success">Entrou</a>
                </li>
                @endif 
                @endif
                @endforeach
            </ul>
        </div>
        <hr/>
        <div>
            <h2 style="text-align: center">Processos de votação:</h2>
            <div class="createVoteDivi">
                <form method="POST" action="/meetings/votes/{{$meeting->id}}" enctype="application/x-www-form-urlencoded" >
                    {{ csrf_field() }}
                    <h4>Criar processo de votação:</h4>
                    <input type="checkbox" name="createVote" class="createVote">
                    @if (session()->has('voteError'))
                    <div style="color: red">{{session('voteError')}}</div>
                    @endif
                    <div class="createVoteDiv">
                        <p>Pergunta da votação: <input style="color: black;" type="text" name="name" required></p>
                        <b>Opções de resposta:</b>
                        <div class="options">
                            <p>Opção 1: <input style="color: black;" type="text" name="option1" required></p>
                            <p>Opção 2: <input style="color: black;" type="text" name="option2" required></p>
                        </div>
                        <button class="btn btn-danger removeOption">Remover opção</button>
                        <button class="btn btn-success addOption">Adicionar opção</button>
                        <div class="center btnSave">
                            <button class="btn btn-info" type="submit">Criar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="createdVotes">
            @foreach($meeting->votes as $index => $vote)
            <div class="createdVote">
                <h4><b>Votação {{ $index+1 }}</b></h4>
                <p><b>Pergunta:</b>{{ $vote->name }}</p>
                <p><b>Estado: </b>{{ $vote->stateToStr() }}</p>
                @if($vote->state == 0)
                {{-- Agendada --}}
                <a href="{{ url('/meetings/' .$meeting->id. '/startVote/' . $vote->id) }}" class="btn btn-success">Começar votação</a>
                <p>Apenas os utilizadores presentes no momento terão acesso à votação!</p>
                @elseif($vote->state == 1)
                {{-- A decorrer --}}
                <p><b>Votos: {{$vote->votes}}</b></p>
                <a href="{{ url('/meetings/' .$meeting->id. '/endVote/' . $vote->id) }}" class="btn btn-info">Terminar votação</a>
                @else
                {{-- Finalizada --}}
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>
<hr>
</div>
@endif
<div class="alert alert-info" role="alert">
    {{-- PRECENCA MARCADA --}}
    @if($pivot->present == 1)
    @if($pivot->presentNow == 1)
    <div style="text-align: center">
        <h3>Está presente na reunião!</h3>
        <p>Aqui poderá aceder aos processos de votação e muito mais!</p>
    </div>
    
    {{-- VOTAÇÕES --}}
    
    <div>
        <div class="votes">
            @foreach ($meeting->votes as $index => $vote)
            @if ($vote->state == 1 && $user->votes->find($vote->id))
            <div class="jumbotron vote">
                <h3 class="display-4"><b>Pergunta:</b> {{ $vote-> name }}</h3>
                <p class="lead"><b>Estado:</b> {{ $vote->stateToStr() }}</p>
                <hr class="my-4">
                @if($user->votes->find($vote->id)->pivot->voted == 1)
                <span>Já votou nesta votação!</span>
                @else
                <form method="POST" action="/meetings/{{$meeting->id}}/vote/{{$vote->id}}" enctype="application/x-www-form-urlencoded" >
                    {{ csrf_field() }}
                    @foreach ($vote->options as $index => $option)
                    <input type="radio" name="option" value="{{$option->id}}">{{ $option->name }}<br>
                    @endforeach
                    <button class="btn btn-primary" type="submit">Votar</button>
                </form>
                @endif
            </div>
            @elseif($vote->state == 2)
            {{-- Votação concluida --}}
            <div class="jumbotron vote voteDone">
                <h3 class="display-4"><b>Pergunta:</b> {{ $vote-> name }}</h3>
                <p class="lead"><b>Estado:</b> {{ $vote->stateToStr() }}</p>
                <hr class="my-4">
                <b>Resultados:</b>
                @foreach ($vote->options as $index => $option)
                <br><b>{{$option->name}}</b>
                <progress value="{{$option->votes}}" max="{{$vote->votes}}"></progress> ({{ $option->votes }} votes)
                @endforeach
            </div>
            
            
            @endif
            
            @endforeach
        </div>
    </div>
    @else
    <div>
        <b>Foi marcado como não estando presente. Se esta informação não estiver correta contacte o administrador ou secretário da reunião.</b>
    </div>
    @endif
    @else
    {{-- MARCAR PRESENCA --}}
    <div>
        <h3>Marcar presença:</h3>
        <p>Este código será dado pelo presidente ou secretário da reunião.</p>
        <form method="POST" action="/meetings/present/{{$meeting->id}}" enctype="application/x-www-form-urlencoded" >
            {{ csrf_field() }}
            <p>Código: <input type="text" name="code" required></p>
            @if(session()->has('codeError'))
            <div class="alert alert-danger">
                {{ session()->get('codeError') }}
            </div>
            @endif
            <button class="btn btn-primary" type="submit">Enviar</button>
        </form>
    </div>
    <br>
    @endif
    @if($user->meetings->find($meeting->id)->pivot->role == 1 || $user->meetings->find($meeting->id)->pivot->role == 2 && $meeting->state == 1)
    <div class="form-group" id="show-m">
        <input type="hidden" value="{{$meeting->id}}" ref="meeting">
        <input type="hidden" value="{{$meeting->location}}" ref="location">
        <input type="hidden" value="{{$meeting->organism->name}}" ref="organism">
        {{ csrf_field() }}
        <div class="form-group">
            <h3>Elaboração de Ata</h3>
        </br>
        <h4>Introdução da ata</h4>
        <form method="POST" action="/meeting/main/{{$meeting->id}}" enctype="application/x-www-form-urlencoded">
            {{ csrf_field() }}
            <div class="row">
                <div class="form-group col-sm-3 @if ($errors->has('filename_img')) has-error @endif">
                    <label for="meetingStatus">Selecione o logo da ata</label>
                    @if ($errors->has('filename_img'))
                        <br>
                        <span class="error" style="color:darkred">Selecione um logo.</span>
                    @endif
                    <select id="meetingStatus" name="filename_img" class="form-control col-sm-3">
                        <option value="" disabled selected>Selecione um ficheiro</option>
                        @foreach(\Illuminate\Support\Facades\Storage::files('/public/logos') as $index => $filename)
                        <option @if (old('filename_img') == $filename) selected="selected" @endif value="{{$filename}}">{{$filename}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-3">
                    <select class="form-control" id="template" v-model="template" name="point_template">
                        @foreach($templates as $template)
                        <option value="{{$template->id}}">{{$template->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-sm-1">
                    <a class="btn btn-success" @click="getTemplateContentToMain()"><i class="glyphicon glyphicon-plus"></i></a>
                </div>
            </div>
            <div class="form-group tiny">
                <editor api-key="u1p3byktyqznf8bi229hakncdpkxe65vm0aieikszs3d6wk1" :init="{plugins: 'wordcount', height: 300, width: 1000}" v-model="main.parent" name="text_main"></editor>
                <br>
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i>  Submeter introdução</button>
            </div>
        </form>
    </div>
    <br>
    <div class="form-group">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        @foreach($meeting->points as $index => $point)
        <input type="hidden" value="{{$point->id}}" ref="point{{$point->id}}">
        <input type="hidden" value="{{$index}}" ref="index{{$index}}">
        <div class="clearfix">
            <span class="pull-left">
                <div class=" btn-group">
                    <button class="btn btn" type="button">
                        <span class="fa fa-pencil-square"></span> {{$point->name}}
                    </button>
                    <button class="btn btn btn-primary showhidereply" type="button" :data-id="{{$index}}" @click="getDescriptionFromPoint({{$index}}, {{$point->id}})">
                        <span class="fa fa-eye"></span>
                    </button>
                </div>
            </span>
        </div>
        
        <!-- Ponto -->
        <div class="form-group" id="replycomment-{{$index}}" hidden>
            <hr class="normal">
            <label>Ata para o ponto {{$point->name}}</label>
            
            <br>
            <div class="row">
                <div class="form-group col-sm-3">
                    <select class="form-control" id="template" v-model="template" name="point_template">
                        @foreach($templates as $template)
                        <option value="{{$template->id}}">{{$template->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-sm-1">
                    <button class="btn btn-success" type="button" @click="getTemplateContent({{$index}})"><i class="glyphicon glyphicon-plus"></i></button>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-1">
                    <button class="btn btn-success subpoint" type="button" @click="addRow({{$index}})"><i class="glyphicon glyphicon-plus"></i> Adicionar Subponto</button>
                </div>
            </div>
            <br>
            <div class="form-group">
                <form method="POST" action="/point/description/{{$point->id}}" enctype="application/x-www-form-urlencoded">
                    {{ csrf_field() }}
                    <div class="form-group tiny">
                        <editor api-key="u1p3byktyqznf8bi229hakncdpkxe65vm0aieikszs3d6wk1" :init="{plugins: 'wordcount', height: 300, width: 1000}" v-model="form.parent_id[{{$index}}]" :name="getPointTinyName({{$index}}, 'point')"></editor>
                        <br>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Submeter Ponto</button>
                    </div>
                </form>
                <!-- Subponto -->
                @foreach($subpoints[$index]->subpoints as $i => $subpoint)
                <div class="clearfix">
                    <span class="pull-left">
                        <div class=" btn-group">
                            <button class="btn btn" type="button">
                                <span class="fa fa-pencil-square"></span> {{$subpoint->name}}
                            </button>
                            <button class="btn btn btn-primary showSubpoint" type="button" :data-id="{{$i}}" @click="getDescriptionFromSubpoint({{$i}}, {{$subpoint->id}}); getNameFromSubpoint({{$i}}, {{$subpoint->id}})">
                                <span class="fa fa-eye"></span>
                            </button>
                        </div>
                    </span>
                </div>
                <br>
                <div class="form-group" id="replySubpoint-{{$i}}" hidden>
                    <div class="form-group col-sm-3">
                        <select class="form-control" id="template" v-model="template" name="point_template">
                            <option disabled>Selecione o template que deseja</option>
                            @foreach($templates as $template)
                            <option value="{{$template->id}}">{{$template->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-1">
                        <button class="btn btn-success point" type="button" @click="getTemplateSubpoint({{$i}})"><i class="glyphicon glyphicon-plus"></i></button>
                    </div>
                    <form method="POST" action="/update/subpoint/{{$subpoint->id}}" enctype="application/x-www-form-urlencoded">
                        {{ csrf_field() }}
                        <span class="input-group-addon">
                            <div class="form-group">
                                <input type="text" class="form-control" v-model="sub.name[{{$i}}]" :name="getSubpointTinyName({{$point->id}}, 'name')">
                            </div>
                            <div class="form-group tiny">
                                <editor api-key="u1p3byktyqznf8bi229hakncdpkxe65vm0aieikszs3d6wk1" :init="{plugins: 'wordcount', height: 300, width: 1000}" v-model="sub.subpoint[{{$i}}]" :name="getSubpointTinyName({{$point->id}}, 'subpoint')"></editor>
                                <br>
                            </div>
                        </span>
                        <br>
                        <button class="btn btn-success" type="submit" >Submeter subponto</button>
                    </form>
                </div>
                @endforeach
                <div class="form-group" v-for="subpoint in subpoints">
                    <div class="form-group" v-if="subpoint.index === {{$index}}">
                        <div class="form-group col-sm-3">
                            <select class="form-control" id="template" v-model="template" name="point_template">
                                <option disabled>Selecione o template que deseja</option>
                                @foreach($templates as $template)
                                <option value="{{$template->id}}">{{$template->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-1">
                            <button class="btn btn-success point" type="button" @click="getTemplateContentSubpoint(subpoint)"><i class="glyphicon glyphicon-plus"></i></button>
                        </div>
                        <form method="POST" action="/point/subpoint/{{$point->id}}" enctype="application/x-www-form-urlencoded">
                            {{ csrf_field() }}
                            <span class="input-group-addon">
                                <div class="form-group">
                                    <input type="text" class="form-control" v-model="subpoint.name" placeholder="Ex: a) Votação xpto" :name="getSubpointTinyName({{$point->id}}, 'name')">
                                </div>
                                <div class="form-group tiny">
                                    <editor api-key="u1p3byktyqznf8bi229hakncdpkxe65vm0aieikszs3d6wk1" :init="{plugins: 'wordcount', height: 300, width: 1000}" v-model="subpoint.label" :name="getSubpointTinyName({{$point->id}}, 'subpoint')"></editor>
                                    <br>
                                </div>
                            </span>
                            <button class="btn btn-success" type="submit" >Submeter subponto</button>
                        </form>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <br>
        @endforeach
    </div>
    <div class="form-group">
        <h4>Conclusão da ata</h4>
        <form method="POST" action="/meeting/conclusion/{{$meeting->id}}" enctype="application/x-www-form-urlencoded">
            {{ csrf_field() }}
            <div class="row">
                <div class="form-group col-sm-3">
                    <select class="form-control" id="template" v-model="template" name="point_template">
                        @foreach($templates as $template)
                        <option value="{{$template->id}}">{{$template->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-sm-1">
                    <a class="btn btn-success" @click="getTemplateContentToConclusion()"><i class="glyphicon glyphicon-plus"></i></a>
                </div>
            </div>
            <div class="form-group tiny">
                <editor api-key="u1p3byktyqznf8bi229hakncdpkxe65vm0aieikszs3d6wk1" :init="{plugins: 'wordcount', height: 300, width: 1000}" v-model="conclusion" name="conclusion"></editor>
                <br>
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i>  Submeter Conclusão</button>
            </div>
        </form>
    </div>
</div>
@endif
</div>
</div>
@endif

{{-- REUNIÃO FINALIZADA --}}
{{-- SE AINDA NAO HOUVER ATA ASSINADA, APARECE FORM PARA FAZER UPLOAD, SE JÁ TIVER APAREÇE BOTÃO DOWNLOAD ATA ASSINADA --}}
<div class="center">
    @if($meeting->state == 2 && $user->meetings->find($meeting->id)->pivot->role == 1 || $user->meetings->find($meeting->id)->pivot->role == 2)
    <br><br>
    <h2>Por favor, faça o upload da ata assinada.</h2>
    <div class="center middle">
        <form action="/storage/ata/{{$meeting->id}}" enctype="multipart/form-data" method="post">
            {{ csrf_field() }}
            <input style="margin: auto; padding-bottom: 5px;" type="file" name="ata">
            <button type="submit" class="btn btn-info"><i class="fa fa-upload"></i>  Upload ata</button>
        </form>
    </div>
    <br>
    @endif
    @if(Storage::exists('/public/'.$meeting->id) && $meeting->state == 2)
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-download"></i>  Download de ata</button>
    <div class="modal fade" id="myModal" role="dialog" style="padding-top: 250px;" >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Obter extrato de ata</h4>
                </div>

                <form method="POST" action="/download/ata" enctype="application/x-www-form-urlencoded" >
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="meetingStatus">Selecione a ata ou extrato:</label>
                            <select id="meetingStatus" name="filename" class="form-control col-sm-3">
                                <option value="" disabled selected>Selecione um ficheiro</option>
                                @foreach(\Illuminate\Support\Facades\Storage::files('/public/'.$meeting->id) as $filename)
                                <option value="{{$filename}}">{{$filename}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success"><i class="fa fa-download"></i>  Download</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    @endif
</div>

<style>
    
    .presences {
        width: 35%;
        display: inline-block;
        vertical-align: top;
    }
    .presencesNow {
        padding-left: 30px;
        width: 55%;
        display: inline-block;
        border-left: 1px solid grey;
        vertical-align: top;
    }
    .btnSave {
        padding-top: 10px;
    }
    .middle {
        margin-left: auto;
        margin-right: auto;
    }
    .rejectDiv > label {
        padding-top: 15px;
    }
    .subtitle {
        margin-top: 0;
    }
    .center {
        text-align: center;
        
    }
    
    .createdVotes {
        margin-top: 20px;
        text-align: center;
    }
    
    .createdVote {
        margin-top: 20px;
        width: 40%;
        display: inline-block;
        padding: 15px;
        margin-right: 20px;
        box-sizing: border-box;
        min-height: 100px;
        background-color: #c5a372;
        border-radius: 10px;
        color: white;
        vertical-align:top;
    }
    
    .createVoteDivi {
        background-color: #956f3a;
        border-radius: 10px;
        color: white;
        width: 70%;
        margin: auto;
        text-align: center;
    }
    .infoTable {
        width: 40%;
        margin-left: auto;
        margin-right: auto;
    }
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        
    }
    
    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    
    tr:nth-child(even) {
        background-color: #dddddd;
    }
    
    .lead {
        padding-top: 20px;
    }
    .votes {
        text-align: center;
    }
    .vote {
        width: 40%;
        display: inline-block;
        padding: 15px;
        margin-right: 20px;
        box-sizing: border-box;
        min-height: 300px;
        vertical-align:top;
    }
    
    @media (max-width: 767px) {
        .vote {
            width: 90%;
            display: block;
            padding: 15px;
            margin-right: 0;
            box-sizing: border-box;
            min-height: 300px;
            vertical-align:top;
            margin: auto;
            margin-bottom: 10px;
        }
    }
    
    .voteDone {
        background-color: #d7d7d7;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(function(){
        // change the selector to use a class
        $(".showhidereply").click(function(){
            // this will query for the clicked toggle
            var $toggle = $(this);
            // build the target form id
            var id = "#replycomment-" + $toggle.data('id');
            console.log(id);
            
            $( id ).toggle();
        });
    });
    
    $(function(){
        // change the selector to use a class
        $(".showSubpoint").click(function(){
            // this will query for the clicked toggle
            var $toggle = $(this);
            // build the target form id
            var id = "#replySubpoint-" + $toggle.data('id');
            console.log(id);
            
            $( id ).toggle();
        });
    });
</script>
<script src="{{ asset('js/showMeeting.js') }}"></script>
<script>
    $("#myID").click(
    function () {
        var someText = "my dynamic text";
        var newDiv = $("<div>").append(someText).click(function () { alert("click!"); });
            $(this).append(newDiv);
        }
        )
    </script>
    <script>
        var counter = 2;
        $('.rejectMeetingDiv').hide();
        $('.definePresident').hide();
        $('.createVoteDiv').hide();
        $(".removeOption").attr("disabled", true);
        $('.rejectMeetingCheck').change(function(){
            $('.rejectMeetingDiv').toggle();
        });
        $('.definePresidentCheck').change(function(){
            $('.definePresident').toggle();
        });
        
        $('.createVote').change(function(){
            $('.createVoteDiv').toggle();
        });
        
        $('.addOption').click(() => {
            counter = counter+1;
            $( ".options" ).append(`<p>Opção ${counter}: <input style="color: black; type="text" name="option${counter}" required></p>`);
            if(counter > 2) {
                $(".removeOption").attr("disabled", false);
            }
        });
        
        $('.removeOption').click(() => {
            $('.options p').last().remove();
            counter = counter-1;
            if(counter <= 2) {
                $(".removeOption").attr("disabled", true);
            }
        });
    </script>
    <script>
        $(".alert-success").fadeTo(5000, 500).slideUp(500, function(){
            $(".alert-success").slideUp(500);
        });
        $(".alert-danger").fadeTo(5000, 500).slideUp(500, function(){
            $(".alert-danger").slideUp(500);
        });
    </script>
    <script>
        function getHTMLContent() {
            
            Conteudo = tinyMCE.get("Conteudo").getContent();
            ConteudoEN = tinyMCE.get("ConteudoEN").getContent();
            ConteudoES = tinyMCE.get("ConteudoES").getContent();
        }
        
        tinymce.init({
            selector:'.conteudo',
            force_br_newlines : true,
            force_p_newlines : false,
            forced_root_block : '',
            font_formats: 'Arial=arial',
            fontsize_formats: '11pt',
            align: 'justify',
            height: 300,
            width: 1000,
            theme: 'modern',
            content_style: "div {margin-bottom: 4.72pc; margin-left: 7pc; margin-right: 13pc; text-align: justify; word-spacing: 1px; font-size: 15px; line-height: 1.5; text-indent: 50px;} p{ margin: 0; padding: 0;}",
            plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link upload image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            image_advtab: true,
            templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css',
            ],
            textcolor_map: [
            "993300", "Burnt orange",
            "333300", "Dark olive",
            "003300", "Dark green",
            "003366", "Dark azure",
            "000080", "Navy Blue",
            "333399", "Indigo",
            "333333", "Very dark gray",
            "800000", "Maroon",
            "FF6600", "Orange",
            "808000", "Olive",
            "008000", "Green",
            "008080", "Teal",
            "0000FF", "Blue",
            "666699", "Grayish blue",
            "808080", "Gray",
            "FF0000", "Red",
            "FF9900", "Amber",
            "99CC00", "Yellow green",
            "339966", "Sea green",
            "33CCCC", "Turquoise",
            "3366FF", "Royal blue",
            "800080", "Purple",
            "999999", "Medium gray",
            "FF00FF", "Magenta",
            "FFCC00", "Gold",
            "FFFF00", "Yellow",
            "00FF00", "Lime",
            "00FFFF", "Aqua",
            "00CCFF", "Sky blue",
            "993366", "Red violet",
            "FFFFFF", "White",
            "FF99CC", "Pink",
            "FFCC99", "Peach",
            "FFFF99", "Light yellow",
            "CCFFCC", "Pale green",
            "CCFFFF", "Pale cyan",
            "99CCFF", "Light sky blue",
            "CC99FF", "Plum",
            "DD1A22", "Afaloc Vermelho",
            "9CCCE4", "Alfaloc Azul"
            ]
        });
    </script>
    @endsection