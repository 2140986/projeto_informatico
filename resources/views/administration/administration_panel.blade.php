@extends('layouts.app')

@section('content')
<div id="page-wrapper" xmlns:v-on="http://www.w3.org/1999/xhtml">
    <h1>Administration Panel</h1>
    <div>
        <h2>Orgãos:</h2>
        <div class="alert alert-success" role="alert">
            @foreach ($organisms as $key => $organism)
            <div>
                <h3><b>Nome: </b>{{$organism->name}}</h3>
                <h3>Membros:</h3>
                <form method="POST" action="/organisms/positions/{{$organism->id}}" enctype="application/x-www-form-urlencoded" >
                    {{ csrf_field() }}
                    <table class="middle" style="width: 100%;">
                        <tr>
                            <th>Nome</th>
                            <th>Posição</th>
                            <th>Ações</th>
                        </tr>
                        @foreach ($organism->users as $key => $userC)
                        <tr>
                            <td>{{$userC->name}}</td>
                            <td>
                                <select class="form-control form-control-line" name="{{$userC->id}}">
                                    <option id="position" @if($userC->organisms->find($organism->id)->pivot->position === 0) selected @endif value="0">Membro</option>
                                    <option id="position" @if($userC->organisms->find($organism->id)->pivot->position === 1) selected @endif value="1">Presidente</option>
                                    <option id="position" @if($userC->organisms->find($organism->id)->pivot->position === 2) selected @endif value="2">Presidente Suplente</option>
                                    <option id="position" @if($userC->organisms->find($organism->id)->pivot->position === 3) selected @endif value="3">Secretário</option>
                                </select>
                            </td>
                            <td>
                                <a href="/organisms/{{$organism->id}}/removeMember/{{$userC->id}}" class="btn btn-danger">Remover</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <button class="btn btn-info" type="submit">Guardar</button>
                </form>
            </div>
            <div>
                <h3>Adicionar membro:</h3>
                <form method="POST" action="/administration/addMember/{{$organism->id}}" enctype="application/x-www-form-urlencoded" >
                    {{ csrf_field() }}
                    <p>Email: <input type="text" name="email" required></p>
                    @if(session()->has('addMemberError') && isset(session('addMemberError')[$organism->id]))
                    <div style="color: red;">{{session('addMemberError')[$organism->id]}}</div>
                    @endif
                    <button class="btn btn-primary" type="submit">Adicionar</button>
                </form>
            </div>
            <hr/>
            @endforeach
        </div>
        <div>
            <h3>Criar orgão:</h3>
            <form method="POST" action="/administration/createOrganism" enctype="application/x-www-form-urlencoded" >
                {{ csrf_field() }}
                <p>Nome: <input type="text" name="name" required></p>
                @if (session('createOrganismError'))
                {{-- @if(isset($createOrganismError)) --}}
                <div style="color: red;">{{ session('createOrganismError') }} {{-- {{$createOrganismError}} --}}</div>
                @endif
                <button class="btn btn-primary" type="submit">Criar</button>
            </form>
        </div>
    </div>
    @if(session()->has('message'))
        <div class="alert alert-success message">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger error">
            {{ session()->get('error') }}
        </div>
    @endif
    <div>
        <h2>Templates:</h2>
        <div class="alert alert-warning" role="alert">
            @foreach ($templates as $key => $template)
            <div>
                <h3><b>Titulo: </b>{{$template->title}}</h3>
                <h3>Conteúdo:</h3>
                <div class="alert alert-info" role="alert">
                    {!!$template->content!!}
                </div>
            </div>
            @endforeach
            {{ $templates->links() }}
        </div>
        <form method="POST" action="/store/template" enctype="application/x-www-form-urlencoded">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Escreva template que deseja realizar</label>
                <div class="form-group">
                    <input id="name" class="form-control col-sm-6" name="title" placeholder="Insira o titulo do template">
                </div>
                <br><br>
                <div class="form-group">
                    <textarea id="conteudo" name="conteudo" class="form-control conteudo"></textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Criar Template</button>
        </form>
    </div>
</div>
<!-- /.container-fluid -->
<footer class="footer text-center"> 2018 &copy; Instituto Politécnico de Leiria </footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .middle {
        margin-left: auto;
        margin-right: auto;
        overflow-y: auto;
    }
    .center {
        text-align: center;
        
    }
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        
    }
    
    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    tr {
        background-color: white;
    }
    
    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<script>
    $(".message").fadeTo(5000, 500).slideUp(500, function(){
        $(".message").slideUp(500);
    });

    $(".error").fadeTo(5000, 500).slideUp(500, function(){
        $(".error").slideUp(500);
    });
</script>
<script>
    function getHTMLContent() {

        Conteudo = tinyMCE.get("Conteudo").getContent();
        ConteudoEN = tinyMCE.get("ConteudoEN").getContent();
        ConteudoES = tinyMCE.get("ConteudoES").getContent();
    }

    tinymce.init({
        selector:'.conteudo',
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : '',
        font_formats: 'Arial=arial',
        fontsize_formats: '11pt',
        align: 'justify',
        height: 300,
        width: 800,
        theme: 'modern',
        content_style: "div {margin-bottom: 4.72pc; margin-left: 7pc; margin-right: 13pc; text-align: justify; word-spacing: 1px; font-size: 15px; line-height: 1.5; text-indent: 50px;} p{ margin: 0; padding: 0;}",
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link upload image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css',
        ],
        textcolor_map: [
            "993300", "Burnt orange",
            "333300", "Dark olive",
            "003300", "Dark green",
            "003366", "Dark azure",
            "000080", "Navy Blue",
            "333399", "Indigo",
            "333333", "Very dark gray",
            "800000", "Maroon",
            "FF6600", "Orange",
            "808000", "Olive",
            "008000", "Green",
            "008080", "Teal",
            "0000FF", "Blue",
            "666699", "Grayish blue",
            "808080", "Gray",
            "FF0000", "Red",
            "FF9900", "Amber",
            "99CC00", "Yellow green",
            "339966", "Sea green",
            "33CCCC", "Turquoise",
            "3366FF", "Royal blue",
            "800080", "Purple",
            "999999", "Medium gray",
            "FF00FF", "Magenta",
            "FFCC00", "Gold",
            "FFFF00", "Yellow",
            "00FF00", "Lime",
            "00FFFF", "Aqua",
            "00CCFF", "Sky blue",
            "993366", "Red violet",
            "FFFFFF", "White",
            "FF99CC", "Pink",
            "FFCC99", "Peach",
            "FFFF99", "Light yellow",
            "CCFFCC", "Pale green",
            "CCFFFF", "Pale cyan",
            "99CCFF", "Light sky blue",
            "CC99FF", "Plum",
            "DD1A22", "Afaloc Vermelho",
            "9CCCE4", "Alfaloc Azul"
        ]
    });
</script>
@endsection