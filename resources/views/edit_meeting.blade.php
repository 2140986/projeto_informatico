@extends('layouts.app')

@section('content')
    <div id="page-wrapper" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-8 col-md-4 col-sm-4 col-xs-12">
                    <br>
                    <h4 class="page-title">Editar Reunião para o planetário de {{$organism->name}}</h4> </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box" id="organisms">
                        <form class="form-horizontal form-material" method="POST" action="/update/meeting/{{$meeting->id}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input v-model="member" name="meeting_member" value="{{$organism->id}}" hidden>
                                <input v-model="memberName" name="organism_name" value="{{$organism->name}}" hidden>

                                <label class="col-md-12" style="color: #85b4d0;">Tipo*</label>
                                <div class="col-md-3">
                                    <select class="form-control form-control-line" name="type">
                                        @if($meeting->type == 1)
                                            <option id="meeting_member" value="1" selected>Ordinária</option>
                                            <option id="meeting_member" value="2">Extraordinária</option>
                                        @else
                                            <option id="meeting_member" value="2" selected>Extraordinária</option>
                                            <option id="meeting_member" value="1" selected>Ordinária</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" style="color: #85b4d0;">A sua função</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control form-control-line" name="description" v-model="description_r">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="meeting_date" class="col-md-3" style="color: #85b4d0;">Data*</label>
                                <label for="meeting_start" class="col-md-3" style="color: #85b4d0;" >Hora de Ínicio*</label>
                                <label for="meeting_end" class="col-md-3" style="color: #85b4d0;">Duração</label>
                                <label for="meeting_classroom" class="col-md-3" style="color: #85b4d0;">Local*</label>
                                <div class="col-md-3">
                                    <input type="date" class="form-control form-control-line"  name="meeting_date" id="meeting_date" value="{{ $meeting->date->format('Y-m-d') }}">
                                </div>
                                <div class="col-md-3">
                                    <input type="time" class="form-control form-control-line"  name="meeting_start" id="meeting_start" value="{{ $meeting->initHour }}">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control form-control-line" name="meeting_end" id="meeting_end" value="{{ $meeting->duration }}">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control form-control-line" name="meeting_classroom" id="meeting_classroom" value="{{ $meeting->location }}">
                                </div>
                            </div>
                            <div class="form-group" id="orders">
                                <label class="col-md-12" style="color: #85b4d0;">Ordem de trabalhos*</label>

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Pontos</th>
                                        <th>
                                            <a class="btn btn-success btn-xs" id="addRowToTable" name="buttonAdd">
                                                <span class="glyphicon glyphicon-plus"></span></a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($points as $index=>$point)
                                    <tr v-for="(row, index) in rows">
                                        <td>
                                            {{$index +1 }}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control col-xs-6" id="numberVolumes"
                                                    :name="getInputName({{$index}}, 'number')" required value="{{$point->name}}">
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-xs" @click="removeRow(index)"><span class="glyphicon glyphicon-minus"></span></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <div class="form-group">
                                <label class="col-md-12" style="color: #85b4d0;">Convocados*</label>
                                <div class="col-md-3 border-class">
                                    <div class="col-md-offset-1">
                                        @foreach($users as $index=>$user)
                                            <div class="form-group">
                                                <input type="checkbox" :value="{{$user->id}}" id="user_summoned" :name="getMembersName({{$index}}, 'id')">
                                                <label  for="user_summoned" style="color: rgba(0,0,0,.5); padding-left: 10px;">{{$user->name}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label style="color: #85b4d0;">Conteúdo para Email da convocatória (alterações aconselhadas)</label>
                                <textarea name="email" class="form-control conteudo">
                                    <p>Caros colegas,</p>
                                    <p>Ao abrigo do artº 44º dos Estatutos da Escola Superior de Tecnologia e  Gestão de Instituto Politécnico de Leiria,
                                        venho por este meio convocar  uma reunião do Plenário do Departamento de {{$organism->name}}
                                        para as {{$meeting->initDate}} do dia {{$meeting->date->format('d/m/Y (D)')}}, na {{$meeting->classroom}},  com a seguinte ordem de trabalhos:</p>
                                        <ol>
                                            @foreach($points as $point)
                                              <li>{{$point->name}}</li>
                                            @endforeach
                                        </ol>

                                    <p>A reunião tem uma duração máxima prevista de {{$meeting->endDate}}</p>
                                    <p>Cumprimentos, <br>
                                    {{ \Illuminate\Support\Facades\Auth::user()->name }}</p>
                                    <p>Escola Superior de Tecnologia e Gestão<br>
                                        Instituto Politécnico de Leiria</p>
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="attachments" class="col-md-12" style="color: #85b4d0;">Anexar Ficheiros (Opcional)</label>
                                <div class="form-group" style="padding-left: 10px;">
                                    <div class="col-md-6">
                                        <input type="file" name="file" :change="file" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-offset-1">
                                    <div class="col-md-6">
                                        <input class="form-control" v-model="documentation" id="attachments" name="attachments" placeholder="Link para repositório de documentos">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success" type="submit">Submeter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2018 &copy; Instituto Politécnico de Leiria </footer>
    <script src="https://unpkg.com/vue"></script>
    <script src="{{ asset('js/meetings.js') }}"></script>
    <script src="{{asset('tinymce/js/tinymce/tinymce-vue.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=u1p3byktyqznf8bi229hakncdpkxe65vm0aieikszs3d6wk1"></script>
    <script>
        $("#addRowToTable").click(function () {
            $(".table").each(function () {
                var tds = '<tr>';
                jQuery.each($('tr:last td', this), function () {
                    tds += '<td>' + $(this).html() + '</td>';
                });
                tds += '</tr>';
                if ($('tbody', this).length > 0) {
                    $('tbody', this).append(tds);
                } else {
                    $(this).append(tds);
                }
            });
        });
    </script>

    <script>
        function getHTMLContent() {
            Conteudo = tinyMCE.get("Conteudo").getContent();
            ConteudoEN = tinyMCE.get("ConteudoEN").getContent();
            ConteudoES = tinyMCE.get("ConteudoES").getContent();
        }

        tinymce.init({
            selector:'.conteudo',
            height: 200,
            width: 1000,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link upload image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            textcolor_map: [
                "993300", "Burnt orange",
                "333300", "Dark olive",
                "003300", "Dark green",
                "003366", "Dark azure",
                "000080", "Navy Blue",
                "333399", "Indigo",
                "333333", "Very dark gray",
                "800000", "Maroon",
                "FF6600", "Orange",
                "808000", "Olive",
                "008000", "Green",
                "008080", "Teal",
                "0000FF", "Blue",
                "666699", "Grayish blue",
                "808080", "Gray",
                "FF0000", "Red",
                "FF9900", "Amber",
                "99CC00", "Yellow green",
                "339966", "Sea green",
                "33CCCC", "Turquoise",
                "3366FF", "Royal blue",
                "800080", "Purple",
                "999999", "Medium gray",
                "FF00FF", "Magenta",
                "FFCC00", "Gold",
                "FFFF00", "Yellow",
                "00FF00", "Lime",
                "00FFFF", "Aqua",
                "00CCFF", "Sky blue",
                "993366", "Red violet",
                "FFFFFF", "White",
                "FF99CC", "Pink",
                "FFCC99", "Peach",
                "FFFF99", "Light yellow",
                "CCFFCC", "Pale green",
                "CCFFFF", "Pale cyan",
                "99CCFF", "Light sky blue",
                "CC99FF", "Plum",
                "DD1A22", "Afaloc Vermelho",
                "9CCCE4", "Alfaloc Azul"
            ]
        });


    </script>
@endsection