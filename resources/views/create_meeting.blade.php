@extends('layouts.app')

@section('content')
<div id="page-wrapper" xmlns:v-on="http://www.w3.org/1999/xhtml">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-8 col-md-4 col-sm-4 col-xs-12">
                <br>
                <h4 class="page-title">Criar Reunião para o plenário de {{$organism->name}}</h4> </div>
            <!-- /.col-lg-12 -->
        </div>
        <style>
            .error {
                color: #A52A2A;
            }
        </style>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box" id="organisms">
                        <form class="form-horizontal form-material" method="post" action="/stores/meeting/{{$organism->id}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input v-model="member" name="meeting_member" value="{{$organism->id}}" hidden>
                                <input v-model="memberName" name="organism_name" value="{{$organism->name}}" hidden>

                                <label class="col-md-12" style="color: #85b4d0;">Tipo*</label>
                                <div class="col-md-3 @if ($errors->has('type')) has-error @endif">
                                    <select class="form-control form-control-line" name="type">
                                        <option selected disabled value="0" style="color: gainsboro">Selecione o tipo de reunião</option>
                                        <option id="meeting_member" value="1">Ordinária</option>
                                        <option id="meeting_member" value="2">Extraordinária</option>
                                    </select>
                                    @if ($errors->has('type'))
                                        <span class="error">O campo tipo é obrigatório.</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" style="color: #85b4d0;">A sua função</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control form-control-line" name="description" v-model="description_r">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="meeting_date" class="col-md-3" style="color: #85b4d0;">Data*</label>
                                <label for="meeting_start" class="col-md-3" style="color: #85b4d0;" >Hora de Ínicio*</label>
                                <label for="meeting_end" class="col-md-3" style="color: #85b4d0;">Duração*</label>
                                <label for="meeting_classroom" class="col-md-3" style="color: #85b4d0;">Local*</label>
                                <div class="col-md-3 @if ($errors->has('meeting_date')) has-error @endif">
                                    <input type="date" class="form-control form-control-line" v-model="day" name="meeting_date" id="meeting_date">
                                    @if ($errors->has('meeting_date'))
                                        <span class="error">O campo data é obrigatório.</span>
                                    @endif
                                </div>
                                <div class="col-md-3 @if ($errors->has('meeting_start')) has-error @endif">
                                    <input type="time" class="form-control form-control-line" v-model="hour" name="meeting_start" id="meeting_start">
                                    @if ($errors->has('meeting_start'))
                                        <span class="error">O campo hora de inicio é obrigatório.</span>
                                    @endif
                                </div>
                                <div class="col-md-3 @if ($errors->has('meeting_end')) has-error @endif">
                                    <input type="text" class="form-control form-control-line" v-model="endHour" name="meeting_end" id="meeting_end">
                                    @if ($errors->has('meeting_end'))
                                        <span class="error">O campo duração é obrigatório.</span>
                                    @endif
                                </div>
                                <div class="col-md-3 @if ($errors->has('meeting_classroom')) has-error @endif">
                                    <input type="text" class="form-control form-control-line" v-model="classroom" name="meeting_classroom" id="meeting_classroom">
                                    @if ($errors->has('meeting_classroom'))
                                        <span class="error">O campo local é obrigatório.</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group" id="orders">
                                <label class="col-md-12" style="color: #85b4d0;">Ordem de trabalhos*</label>

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Pontos</th>
                                        <th>
                                            <a class="btn btn-success btn-xs" @click="addRow(index)" name="buttonAdd">
                                                <span class="glyphicon glyphicon-plus"></span></a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(row, index) in rows">
                                        <td>
                                            @{{ index + 1 }}
                                        </td>
                                        <td @if ($errors->has('rows.*')) class="has-error" @endif>
                                            <input type="text" class="form-control" id="numberVolumes"
                                                   v-model="row.number" :name="getInputName(index, 'number')">
                                            @if ($errors->has('rows.*'))
                                                <span class="error">O campo ordem de trabalhos é obrigatório.</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-xs" @click="removeRow(index)"><span class="glyphicon glyphicon-minus"></span></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                            <div class="form-group">
                                <label class="col-md-12 " style="color: #85b4d0;">Convocados*</label>
                                <div class="col-md-3 border-class">
                                    <div class="col-md-offset-1">
                                        @foreach($users as $index=>$user)
                                            <div class="form-group">
                                                <input type="checkbox" :value="{{$user->id}}" id="user_summoned" :name="getMembersName({{$index}}, 'id')">
                                                <label  for="user_summoned" style="color: rgba(0,0,0,.5); padding-left: 10px;">{{$user->name}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                @if ($errors->has('membersCheck.*'))
                                    <span class="error">O campo ordem de trabalhos é obrigatório.</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label style="color: #85b4d0;">Conteúdo para Email da convocatória (alterações aconselhadas)</label>
                                <editor api-key="u1p3byktyqznf8bi229hakncdpkxe65vm0aieikszs3d6wk1"
                                        :init="{plugins: 'wordcount', height: 300, width: 1000}" v-model="emailText" name="text">

                                </editor>
                            </div>
                            <div class="form-group">
                                <label for="attachments" class="col-md-12" style="color: #85b4d0;">Anexar Ficheiros (Opcional)</label>
                                <div class="form-group" style="padding-left: 10px;">
                                    <div class="col-md-6">
                                        <input type="file" name="file" :change="file" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-offset-1">
                                    <div class="col-md-6">
                                        <input class="form-control" v-model="documentation" id="attachments" name="attachments" placeholder="Link para repositório de documentos">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success" type="submit">Submeter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<footer class="footer text-center"> 2018 &copy; Instituto Politécnico de Leiria </footer>
<script src="https://unpkg.com/vue"></script>
<script src="{{ asset('js/meetings.js') }}"></script>
<script src="{{asset('tinymce/js/tinymce/tinymce-vue.min.js')}}"></script>

<script>
    function getHTMLContent() {
        Conteudo = tinyMCE.get("Conteudo").getContent();
        ConteudoEN = tinyMCE.get("ConteudoEN").getContent();
        ConteudoES = tinyMCE.get("ConteudoES").getContent();
    }

    tinymce.init({
        selector:'.conteudo',
        height: 200,
        width: 1000,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link upload image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        textcolor_map: [
            "993300", "Burnt orange",
            "333300", "Dark olive",
            "003300", "Dark green",
            "003366", "Dark azure",
            "000080", "Navy Blue",
            "333399", "Indigo",
            "333333", "Very dark gray",
            "800000", "Maroon",
            "FF6600", "Orange",
            "808000", "Olive",
            "008000", "Green",
            "008080", "Teal",
            "0000FF", "Blue",
            "666699", "Grayish blue",
            "808080", "Gray",
            "FF0000", "Red",
            "FF9900", "Amber",
            "99CC00", "Yellow green",
            "339966", "Sea green",
            "33CCCC", "Turquoise",
            "3366FF", "Royal blue",
            "800080", "Purple",
            "999999", "Medium gray",
            "FF00FF", "Magenta",
            "FFCC00", "Gold",
            "FFFF00", "Yellow",
            "00FF00", "Lime",
            "00FFFF", "Aqua",
            "00CCFF", "Sky blue",
            "993366", "Red violet",
            "FFFFFF", "White",
            "FF99CC", "Pink",
            "FFCC99", "Peach",
            "FFFF99", "Light yellow",
            "CCFFCC", "Pale green",
            "CCFFFF", "Pale cyan",
            "99CCFF", "Light sky blue",
            "CC99FF", "Plum",
            "DD1A22", "Afaloc Vermelho",
            "9CCCE4", "Alfaloc Azul"
        ]
    });


</script>
@endsection