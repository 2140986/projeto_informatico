@extends('layouts.app')

@section('style')
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
@endsection

@section('content')<br>
<h1 class="center">O seu calendário</h1>
<hr>
<p class="center">Aqui poderá encontrar o seu calendário, com todas as reuniões para que foi convocado.</p>
<hr>
<div class="center" style="width: 70%; height: 80%;">
    {!! $calendar->calendar() !!}
</div>
<style>
    #calendar {
        min-width: 900px;
    }
    .center {
        text-align: center;
        margin: auto;
    }
</style>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/lang-all.js"></script>
{!! $calendar->script() !!}
@endsection

