@extends('layouts.app')

@section('content')
<br>
<h1 class="center">{{$organism->name}}</h1>
<hr>
<p class="center">Nesta página poderá encontrar os membros, os seus papéis no órgão e todas as reuniões para que foi convocado associadas a este órgão.</p>
<hr>
<div>
    <div>
        <div>
            <h3 class="center">Membros:</h3>
            <div class="middle">
                    <table class="middle" style="width: 50%">
                            <tr>
                                <th>Nome:</th>
                                <th>Função:</th>
                            </tr>
                            @foreach ($organism->users as $key => $userC)
                            <tr>
                                <td>{{$userC->name}}</td>
                                <td>{{$userC->positionToStr($organism->id)}}</td>
                            </tr>
                            @endforeach
                    </table>
            </div>
        </div>
        
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        @if(session()->has('message-error'))
            <br>
            <div class="alert alert-danger">
                {{ session()->get('message-error') }}
            </div>
        @endif
        <div>
                <h3 class="center">Reuniões que foi convocado:</h3>
                <div class="middle">
                        <table class="middle" style="width: 100%">
                                <tr>
                                    <th>Descrição:</th>
                                    <th>Data:</th>
                                    <th>Sua função:</th>
                                    <th>Estado:</th>
                                    <th>Ver:</th>
                                </tr>
                                @foreach ($organism->meetings as $key => $meeting)
                                @if(!$meeting->users()->where('user_id', $user->id)->get()->isEmpty())
                                <tr>
                                    <td>{{$meeting->seqNumber}}º {{$meeting->title}}</td>
                                    <td>{{$meeting->date->format('d/m/Y')}}</td>
                                    <td>{{$user->meetingRoleToStr($meeting->id)}}</td>
                                    <td>{{$meeting->stateToStr()}}</td>
                                    <td>
                                        <a href="/meetings/{{$meeting->id}}" class="btn btn-info" role="button">Ver Reunião</a>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                        </table>
                </div>
            </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
        .middle {
            margin-left: auto;
            margin-right: auto;
            overflow-y: auto;
        }
        .center {
            text-align: center;
            
        }
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            
        }
        
        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        
        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
<script>
    $('div.alert').delay(20000).slideUp(300);
</script>
@endsection

