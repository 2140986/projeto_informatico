<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<div class="form-group extract" style="font-size: 17px; margin-bottom: 7.72pc; margin-left: 15px; margin-right: 10px; line-height: 1.6; font-family: Arial, sans-serif; text-align: justify; text-indent: 50px;">
    <div class="form-group">
        <table class="table" style="border:0;padding:0;margin:0;padding-right:50px;">
            <col width="200">
            <col width="400">
            <thead>
            <tr>
            </tr>
            </thead>
            <tbody>
            <tr>
                @if($meeting->image)
                <td align="left"><img class="col-sm-2" src="{{url($meeting->image) }}" alt="Image" style="width: 100%;"/></td>
                @else
                    <td align="left"><img class="col-sm-2" src="{{url('logo/ipl.png') }}" alt="Image" style="width: 100%;"/></td>
                @endif
                <td style="text-align: center;"><label style="font-size: 14px; font-weight: bold; text-transform: uppercase;" >Departamento de {{$meeting->organism->name}}</label><br><br><label>{{$meeting->seqNumber}} º CONSELHO DE DEPARTAMENTO</label><br><br><label>ATA</label></td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>
    <br>
    @if($meeting->introduction )
    {!! $meeting->introduction !!}
    @endif
    @if($meeting->points)
        @foreach($meeting->points as $point)
            <p style="font-weight: bold;">Ponto {{$point->number}}. {{$point->name}}</p>
            {!! $point->description !!}
            @if($point->subpoints)
                @foreach($point->subpoints as $subpoint)
                    <p style="font-weight: bold"> {{$subpoint->name}}</p>
                    {!! $subpoint->description !!}
                    {!! $subpoint->description !!}
                    {!! $subpoint->description !!}
                @endforeach
            @endif
        @endforeach
    @endif
    <br>
    @if($meeting->conclusion)
        {!! $meeting->conclusion !!}
    @endif
</div>
<style>
    p
    {
        margin: 0;
        padding: 0;
    }
    p::after {
        content: "-";
    }
    p::before {
        content: "-";
    }

</style>