<!--<div class="form-group extract" style="font-size: 17px; margin-bottom: 4.72pc; margin-left: 7.49pc; margin-right: 4.12pc; line-height: 1.5; font-family: Arial, sans-serif">-->
<div class="form-group extract" style="font-size: 17px; margin-bottom: 7.72pc; margin-left: 15px; margin-right: 10px; line-height: 1.6; font-family: Arial, sans-serif; text-align: justify; text-indent: 50px;">
    <div class="form-group">
        <table class="table" style="border:0;padding:0;margin:0;padding-right:50px;">
            <col width="200">
            <col width="400">
            <thead>
            <tr>
            </tr>
            </thead>
            <tbody>
            <tr>
                @if($meeting->image)
                    <td align="left"><img class="col-sm-2" src="{{url($meeting->image) }}" alt="Image" style="width: 100%;"/></td>
                @else
                    <td align="left"><img class="col-sm-2" src="{{url('logo/ipl.png') }}" alt="Image" style="width: 100%;"/></td>
                @endif
                <td style="text-align: center;"><label style="font-size: 14px; font-weight: bold; text-transform: uppercase;" >Departamento de {{$meeting->organism->name}}</label><br><br><label>{{$meeting->seqNumber}} º CONSELHO DE DEPARTAMENTO</label><br><br><label>EXTRATO DA ATA</label></td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>
    <br>
    @if($meeting->introduction)
        {!! $meeting->introduction !!}
    @endif
    @if( $subpoint->point->description)
        {!! $subpoint->point->description!!}
    @endif
    <p>(...)----------------------</p>
    <p style="font-weight: bold"> {{$subpoint->name}}</p>
    {!! $subpoint->description !!}

</div>

<style>
    div {
        font-size: 17px;
        line-height: 1.5;
        font-family: Arial, sans-serif;
        text-align: inherit;
    }
    p
    {
        text-indent: 50px;
        margin: 0;
        padding: 0;
    }

    p::after {
        content: "-";
    }
    p::before {
        content: "-";
    }

</style>