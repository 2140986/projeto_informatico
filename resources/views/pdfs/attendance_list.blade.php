<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div class="form-group page">
    <p style="font-family: Arial; font-size: 25px; font-weight: bold; text-align: center;">Reunião do Planetário de {{$meeting->organism->name}}
        <br>{{$meeting->date->format('j \d\e F \d\e Y')}}</p>
    <p style="text-align: center; font-size: 20px;"><strong>Lista de Presenças</strong></p>
    <div class="col-xs-12">
        <table class="table table-bordered">
            <thead>
            <tr style="height: 40px;">
                <th style="width: 500px;">Nome</th>
                <th style="width: 500px;">Assinatura</th>
            </tr>
            </thead>
            @foreach($users as $index=>$user)
                <tr style="text-align: center">
                    <td style="width: 500px;">{{$index+1}}.  {{$user->name}}</td>
                    @if($user->justificationAccepted)
                        <td style="width: 500px; text-align: center;">*</td>
                    @else
                    <td style="width: 500px;"></td>
                    @endif
                </tr>
            @endforeach
        </table>
    </div>
</div>
<footer>
    <p>* Justificação de Falta</p>
</footer>
<style>
    .table th {
        background-color: #B0C4DE!important
    }
    .table-bordered td,.table-bordered th
    {
        border:1px solid black!important
    }
    table
    {
        border-spacing:0;
        border-collapse:collapse
    }
    .table
    {
        border-collapse:collapse!important
    }
    .table td
    {
        background-color:#ffffff!important;
        height: 30px;
    }

    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    footer { page-break-inside:avoid; page-break-after:auto }
</style>
</body>
</html>
