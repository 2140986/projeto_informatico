@extends('layouts.app')

@section('content')

<div>
    <h1 class="center">Convocatórias</h1>
    <hr>
    <p class="center">Nesta lista poderá encontrar todas as reuniões que foi convocado agendadas ou a decorrer.</p>
    <hr>
    <div class="notifications">
        @foreach($user->meetings as $key => $meeting)
        @if($meeting->state != 2)
        <div class="jumbotron notification">
            <h3 class="display-4"><b>Convocatória de reunião</b></h3>
            <p class="lead">{{$meeting->title}}</p>
            <hr class="my-4">
            <span><b>Date:</b> {{$meeting->dateToStr()}} - {{$meeting->initHourToStr()}}</span>
            <span><b>Papel:</b> {{$user->meetingRoleToStr($meeting->id)}}</span>
            <p class="lead">
                <a class="btn btn-primary btn-lg" href="/meetings/{{+$meeting->id}}" role="button">Ver reunião</a>
            </p>
        </div>
        @endif
        @endforeach
    </div>
</div>
<style>
    .center {
        text-align: center;
    }
    
    .lead {
        padding-top: 20px;
    }
    .notifications {
        text-align: center;
    }
    .notification {
        width: 40%;
        display: inline-block;
        padding: 15px;
        margin-right: 20px;
        box-sizing: border-box;
        height: 300px;
        vertical-align:top;
    }
</style>
@endsection

