let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/meetings.js', 'public/js')
   .js('resources/assets/js/showMeeting.js', 'public/js')
   .sass('resources/assets/sass/tiny.scss', 'public/css');

mix.copy('node_modules/@tinymce/tinymce-vue/lib/browser/tinymce-vue.min.js', 'public/tinymce/js/tinymce');
