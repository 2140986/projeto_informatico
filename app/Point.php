<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
      /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'points';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];


    public function meeting()
    {
        return $this->belongsTo('App\Meeting');
    }

    public function subpoints()
    {
        return $this->hasMany('App\Subpoint');
    }
}
