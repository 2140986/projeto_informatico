<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subpoint extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subpoints';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'point_id', 'line'];

    public function point()
    {
        return $this->belongsTo('App\Point');
    }
}
