<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganismUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'organism_user';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'organism_id', 'position'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function organism()
    {
        return $this->belongsTo('App\Organism');
    }

}
