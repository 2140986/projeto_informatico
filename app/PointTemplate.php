<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointTemplate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'points_templates';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['point_id', 'template_id'];


    public function points()
    {
        return $this->belongsToMany('App\Point');
    }

    public function templates()
    {
        return $this->belongsTo('App\Template');
    }
}
