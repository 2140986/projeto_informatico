<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organism extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'organisms';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'seqNumber', 'lastMinutePageNumber' ];

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('position');
    }

    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }
}
