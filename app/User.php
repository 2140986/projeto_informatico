<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'admin', 'blocked', 'phone', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function organisms()
    {
        return $this->belongsToMany('App\Organism')->withPivot('position');
    }

    public function positionToStr($organismId)
    {
        $position = $this->organisms->find($organismId)->pivot->position;
        switch ($position) {
            case 0:
                return 'Membro';
            case 1:
                return 'Presidente';
            case 2:
                return 'Presidente Suplente';
            case 3:
                return 'Secretário';
        }
    }
    

    public function meetings()
    {
        return $this->belongsToMany('App\Meeting')->withPivot('rejected', 'justification', 'justificationAccepted', 'role', 'present', 'presentNow');
    }

    public function isAdmin()
    {
        return $this->admin == 1;
    }

    public function meetingRoleToStr($meetingId)
    {
        $role = $this->meetings->find($meetingId)->pivot->role;
        switch ($role) {
            case 0:
                return 'Membro';
            case 1:
                return 'Presidente';
            case 2:
                return 'Secretário';
        }
    }

    public function votes()
    {
        return $this->belongsToMany('App\Vote')->withPivot('voted');
    }

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

}
