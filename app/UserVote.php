<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVote extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_votes';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'vote_id', 'voted'];

    public function vote()
    {
        return $this->belongsTo('App\Vote');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
