<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'votes';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'state', 'votes'];

    public function stateToStr() {
        switch ($this->state) {
            case 0:
                return 'Agendada';
            case 1:
                return 'A decorrer';
            case 2:
                return 'Finalizada';
        }
    }
    
    public function organism()
    {
        return $this->belongsTo('App\Organism');
    }

    public function meeting()
    {
        return $this->belongsTo('App\Meeting');
    }

    public function options()
    {
        return $this->hasMany('App\Option');
    }

    public function point()
    {
        return $this->belongsTo('App\Point');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('voted');
    }
}
