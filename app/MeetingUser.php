<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class MeetingUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'meeting_user';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'meeting_id', 'rejected', 'justification', 'justificationAccepted', 'role'];


    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function meetings()
    {
        return $this->belongsTo('App\Meeting');
    }

}