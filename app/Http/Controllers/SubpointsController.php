<?php

namespace App\Http\Controllers;

use App\Subpoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class SubpointsController extends Controller
{
    /**
     * Store a new subpoint in DB
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeSubpoint($id) {
        $subpoints = Input::get('subpointTiny');

        $i = 0;
        foreach ($subpoints as $subpoint) {
                 $i++;
                $sub = new Subpoint();
                $sub->point_id = $id;
                $sub->name = $subpoint['name'];
                $sub->line = $i;
                $sub->description = $subpoint['subpoint'];
                $sub->save();
        }

        return redirect()->back()->with('message', 'Ponto submetido com sucesso.')->withInput(Input::all());
    }

    /**
     * Store a new subpoint in DB
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSubpoint($id) {
        $subpoints = Input::get('subpointTiny');

        foreach ($subpoints as $subpoint) {
                $sub = Subpoint::find($id);
                $data = [
                    "name" => $subpoint['name'],
                    "description" => $subpoint['subpoint']
                ];

                $sub->update($data);
                return redirect()->back()->with('message', 'Ponto submetido com sucesso.')->withInput(Input::all());
            }

        return redirect()->back()->with('message', 'Ponto submetido com sucesso.')->withInput(Input::all());

    }
}
