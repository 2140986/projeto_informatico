<?php

namespace App\Http\Controllers;

use App\Point;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class PointController extends Controller
{
    /**
     * Store description of point in database
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeDescriptionToPoint($id) {
        $pointTinys = Input::get('pointTiny');

        foreach ($pointTinys as $pointTiny) {
            $point = Point::findOrFail($id);
            $data = [
                'description' => $pointTiny['point']
            ];
            $point->update($data);

        }

        return redirect()->back()->with('message', 'Ponto submetido com sucesso.');

    }

    /**
     * Download of extract of ata
     *
     * @param $id
     * @return mixed
     */
    public function downloadAtaExtract($id) {
        $point = Point::where('id', $id)->first();

        $view = View::make("pdfs.ata_extract",compact("point"));

        $contents = $view->render();

        $pdf = PDF::loadHTML($contents)
            ->setPaper('a4')
            ->setOption('encoding', 'utf-8')
            ->setOption('footer-font-size', 9)
            ->setOption('footer-center', 'Pág. [page] de [toPage]')
            ->setOption('margin-right', 23)
            ->setOption('margin-left', 30);


        return $pdf->inline("Extrato da ata.pdf");
    }
}
