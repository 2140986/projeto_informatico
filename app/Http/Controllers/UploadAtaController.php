<?php

namespace App\Http\Controllers;

use App\Meeting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class UploadAtaController extends Controller
{
    /**
     * Storage ata signed
     *
     * @param Request $request
     * @param $id
     */
    public function storeAta(Request $request, $id) {

        $meeting = Meeting::with('points')->with('organism')->find($id);

        if($request->hasFile('ata')) {
            $request->file('ata');
            if(!Storage::exists('/public/'.$meeting->id)) {
                Storage::makeDirectory('public/'. $meeting->id);
                $request->file('ata')->storeAs('public/'.$meeting->id,  $request->file('ata')->getClientOriginalName());
            } else {
                $request->file('ata')->storeAs('public/'.$meeting->id,  $request->file('ata')->getClientOriginalName());
            }

        } else {
            return redirect()->back()->with('message-error', 'Por favor selecione um ficheiro.');
        }

        return redirect()->back()->with('message', 'O ficheiro foi submetido com sucesso.');

    }

    /**
     * Download ata assinada
     *
     * @param Request $request
     */
    public function downloadAta(Request $request) {

        if(Storage::exists($request->filename)) {
            return Storage::download($request->filename);
        } else {
            return redirect()->back()->with('message-error', 'Por favor selecione um ficheiro.');
        }
    }
}
