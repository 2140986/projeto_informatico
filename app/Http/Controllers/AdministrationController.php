<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use App\Organism;
use App\Template;
use App\User;

class AdministrationController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $organisms = Organism::all();
        $templates = Template::paginate(2);
        if($user->isAdmin()) {
            return view('administration.administration_panel', compact('user', 'organisms', 'templates'));
        }
    }

    public function createOrganism(Request $request) {
        $user = Auth::user();
        $organisms = Organism::all();
        $templates = Template::all();

        if($user->isAdmin()) {
            if($request->name != '' && strlen($request->name) > 3) {
                $organism = new Organism();
                $organism->name = $request->name;
                $organism->lastMinutePageNumber = 0;
                $organism->save();
                $organisms = Organism::all();
                return redirect()->action('AdministrationController@index');
            } else {
                $createOrganismError = "O nome tem de ter mais de 3 caracteres!";
                return redirect('administration')->with('createOrganismError', $createOrganismError); //view('administration.administration_panel', compact('user', 'organisms', 'templates', 'createOrganismError'));
            }
        }
    }

    public function addMemberToOrganism(Request $request, $id) {
        $user = Auth::user();
        $organism = Organism::find($id);
        $userToAdd = User::where('email', $request->email)->first();
        $addMemberError = [];
        if($userToAdd) {
            $exists = $organism->users()->where('user_id', $userToAdd->id)->get();
            if($exists->isEmpty()) {
                $organism->users()->attach($userToAdd, ['position' => 0]);
                $organism->save();
                return redirect()->action('AdministrationController@index');
            } else {
                $addMemberError[$id] = "Email inserido já está presente na lista de membros";
            }
        } else {
            $addMemberError[$id] = "Não foi encontrado nenhum utilizador com o email inserido";
        }
        
        $organisms = Organism::all();
        $templates = Template::all();
        return redirect()->back()->with('addMemberError', $addMemberError);
    }

    public function savePositions(Request $request, $id) {
        $input = $request->all();
        foreach ($input as $key => $value) {
            if($key !== "_token") {
                $user = User::find($key);
                $pivot = $user->organisms->find($id)->pivot;
                $pivot->position = $value;
                $pivot->save();
            }
        }
        return redirect()->action('AdministrationController@index');
    }

    public function removeMember($organismId, $userId) {
        $organism = Organism::find($organismId);
        $organism->users()->detach($userId);
        return redirect()->action('AdministrationController@index');
    }
}
