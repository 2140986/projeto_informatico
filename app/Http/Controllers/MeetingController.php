<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMeetingPost;
use App\Meeting;
use App\MeetingUser;
use App\Notice;
use App\Organism;
use App\OrganismUser;
use App\Point;
use App\PointTemplate;
use App\Subpoint;
use App\Template;
use App\Option;
use App\Vote;
use App\User;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Illuminate\Support\Facades\View;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class MeetingController extends Controller
{
    public function index()
    {
        $events = [];
        $data = Meeting::all();
        if($data->count()) {
            foreach ($data as $d) {
                $events[] = Calendar::event(
                    $d->description,
                    true,
                    $d->date,
                    $d->date,
                    [
                        'color' => '#f05050',
                        'url' => 'pass here url and any route',
                    ]
                );
            }
        }
        $calendar = Calendar::addEvents($events)
            ->setOptions([ //set fullcalendar options
                'lang' => 'pt',
                'editable'=> true,
            ]);  //add an array with addEvents
        return view('fullcalendar', compact('calendar'));
    }


    /**
     * Return view to create Meeting
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createMeeting(Request $request, $id) {

        $organism = Organism::find($id);
        $members = $this->getUserPresidentMembers();
        $users = OrganismUser::where('organism_id', $id)
            ->join('users','users.id','=','user_id')
            ->get();

        return view('create_meeting',
            ["members" => $members,
            "organism" => $organism,
            "users" => $users]);
    }

    /**
     * Get the users that the member is the same that meeting member
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMemberMeeting($id) {

        $users = OrganismUser::where('organism_id', $id)->where('user_id', "!=", Auth::id())
            ->join('users','users.id','=','user_id')
            ->get();

        return $users;
    }

    /**
     * Get the members that the user is president
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUserPresidentMembers() {

        $members = OrganismUser::where('user_id', Auth::id())->where('position', 1)->orWhere('position', 2)
            ->join('organisms','organisms.id','=','organism_id')
            ->get();

        return $members;
    }

    /**
     * Set Attachements Attributes
     *
     * @param Request $request
     * @return bool
     */
    public function setAttachementsAttributes($request) {
        if($request != null) {
            return true;
        }

        return false;
    }
    /**
     * Stores Meeting in BD
     *
     * @param Request $request
     */
    public function storeMeeting(StoreMeetingPost $request, $id) {

        $organism = Organism::find($id);

        $seqNumber = Meeting::where('organism_id', $id)->max('seqNumber');

        $meeting = new Meeting();

        $atach = $this->setAttachementsAttributes($request->file);

        $meeting->title = 'Reunião do Plenário de '.$organism->name;
        $meeting->date = $request->meeting_date;
        $meeting->initHour = $request->meeting_start;
        $meeting->duration = $request->meeting_end;
        $meeting->location = $request->meeting_classroom;
        $meeting->attachments = $atach;
        $meeting->organism_id = $id;
        $meeting->seqNumber = $seqNumber + 1;
        $meeting->state = 0;
        $meeting->type = $request->type;
        $meeting->introduction = "Insira aqui a introdução da ata";
        $meeting->conclusion = "Insira aqui a conclusão da ata";
        $meeting->minute = "nada";
        $meeting->presenceCode = $this->generateRandomString();

        $meeting->save();

        $notice = new Notice();
        $notice->description = $request->text;
        $notice->sendDate = Carbon::now();
        $notice->meeting_id = $meeting->id;

        $notice->save();

        $rows = Input::get('rows');
        $members = Input::get('membersCheck');

        foreach ($members as $member) {
            $user = User::find($member['id']);
            $position = $user->positionToStr($id);

            $meetingUser = new MeetingUser();
            $meetingUser->meeting_id = $meeting->id;
            $meetingUser->user_id = $member['id'];
            if($position == "Presidente" || $position == "Presidente Suplente") {
                $meetingUser->role = 1;
            }elseif ($position == "Membro") {
                $meetingUser->role = 0;
            } else {
                $meetingUser->role = 2;
            }
            $meetingUser->save();
        }

        $i = 0;
        foreach ($rows as $row) {
            $i++;
            $point = new Point();
            $point->number = $i;
            $point->meeting_id = $meeting->id;
            $point->name = $row['number'];
            $point->save();

        }

        $data = [
          "text" => $request->text
        ];

        $file = $request->file;

        Mail::send('mails.convocatoria', ['data' =>  $data], function($m) use($file, $organism, $request, $members) {
            foreach ($members as $member) {
                $user = User::find($member['id']);
                $m->to($user->email);
            }
            $m->from('elabatasipl@gmail.com');
            $m->subject('Convocatória do Plenário de '.$organism->name.' - '.$request->meeting_date.' / '.$request->meeting_start);
            if($file != null) {
                $m->attach($file, array(
                        'as' => $file->getClientOriginalName(), // If you want you can chnage original name to custom name
                        'mime' => $file->getMimeType())
                );
            }

        });
        $request->flash();

        return redirect('/home');
    }

    /**
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function rejectMeeting($id, Request $request) {
        $user = Auth::user();
        $pivot = $user->meetings->find($id)->pivot;
        $pivot->rejected = 1;
        $pivot->justification = $request->justification;
        $pivot->save();
        $meeting = Meeting::find($id);
        return redirect()->back();
        //return view('show_meeting', compact('meeting', 'user'));
    }

    public function editPositions($id, Request $request) {
        $meeting = Meeting::find($id);
        $users = $meeting->users;
        foreach ($users as $user => $value) {
            if($meeting->users->find($value->id)->pivot->role == 2) {
                $pivot = $meeting->users->find($value->id)->pivot;
                $pivot->role = 0;
                $pivot->save();
            }
        }
        $pivot = $meeting->users->find($request->secretary)->pivot;
        $pivot->role = 2;
        $pivot->save();

        if($request->definePresidentCheck == "on") {
            $pivot = $meeting->users->find($request->president)->pivot;
            $pivot->role = 1;
            $pivot->save();
        }

        $user = Auth::user();
        return redirect()->back();
        //return view('show_meeting', compact('meeting', 'user'));
    }

    public function createVote($id, Request $request) {
        $meeting = Meeting::find($id);
        $voteError = "";
        $name = $request->name;
        $numberOfOptions = 0;
        $options = [];
        for ($i=1; $i < 30; $i++) { 
            $value = $request['option'.$i];
            if($value!= null) {
                if (in_array($value, $options)) {
                    $voteError = "Não podem haver opções duplicadas";
                    break;
                }
                $options[$i] = $value;
            }
        }
        
        if($voteError === '') {
            //new Option(['name' => $value, 'votes' => 0]);
            $vote = $meeting->votes()->create(['name' => $name, 'state' => 0, 'votes' => 0]);
            foreach ($options as $key => $value) {
                $vote->options()->create(['name' => $value, 'votes' => 0]);
            }
            
        } 
        $user = Auth::user();
        $pivot = $user->meetings->find($id)->pivot;
        return redirect()->back()->with('voteError', $voteError);
        //return view('show_meeting', compact('meeting', 'user', 'voteError', 'pivot'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $meeting = Meeting::with('points')->with('organism')->find($id);
        $subpoints = Point::where('meeting_id', $id)->with('subpoints')->get();
        $user = Auth::user();
        $pointsTemplates = PointTemplate::join('templates','templates.id','=','template_id')->get();
        $templates = Template::all();
        $pivot = $user->meetings->find($id)->pivot;
        return view('show_meeting', compact('meeting', 'subpoints', 'user', 'templates', 'pointsTemplates', 'pivot'));
    }

    /**
     * Return Edit Meeting View
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editMeetingView($id) {
        $meeting = Meeting::findOrFail($id);
        $organism = Organism::where('id', $meeting->organism_id)->first();
        $points = Point::where('meeting_id', $id)->get();
        $users = OrganismUser::where('organism_id', $meeting->organism_id)
            ->join('users','users.id','=','user_id')
            ->get();

        return view('edit_meeting', [
            'meeting' => $meeting,
            'points' => $points,
            'organism' => $organism,
            'users' => $users]);
    }


    /**
     * Update meeting on database
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateMeeting(Request $request, $id) {

        $meeting = Meeting::findOrFail($id);
        $organism = Organism::where('id', $meeting->organism_id)->first();
        $atach = $this->setAttachementsAttributes($request->file);

        $data = [
            "title" =>'Reunião do Plenário de '.$organism->name,
            "date" => $request->meeting_date,
            "initHour" => $request->meeting_start,
            "duration" =>  $request->meeting_end,
            "location" => $request->meeting_classroom,
            "attachments" => $atach,
            "type" => $request->type
        ];

        $meeting->update($data);

        $notice = new Notice();
        $notice->description = $request->email;
        $notice->sendDate = Carbon::now();
        $notice->meeting_id = $meeting->id;

        $notice->save();

        $rows = Input::get('rows');
        $members = Input::get('membersCheck');

        foreach ($members as $member) {

            $meetingUser = new MeetingUser();
            $meetingUser->meeting_id = $meeting->id;
            $meetingUser->user_id = $member['id'];
            $meetingUser->save();
        }

        $i = 0;
        foreach ($rows as $row) {
            $i++;
            $point = new Point();
            $point->number = $i;
            $point->meeting_id = $meeting->id;
            $point->name = $row['number'];
            $point->save();

        }

        $data = [
            "text" => $request->text
        ];

        $file = $request->file;

        Mail::send('mails.convocatoria', ['data' =>  $data], function($m) use($file, $organism, $request) {
            $m->to('ana.neto96@gmail.com');
            $m->from('elabatasipl@gmail.com');
            $m->subject('Alteração Convocatória do Plenário de '.$organism->name.' - '.$request->meeting_date.' / '.$request->meeting_start);
            if($file != null) {
                $m->attach($file, array(
                        'as' => $file->getClientOriginalName(), // If you want you can chnage original name to custom name
                        'mime' => $file->getMimeType())
                );
            }

        });

        return redirect('/home');

    }

    /**
     * Change status of meeting
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeStatusOfMeeting($id, $status) {
        $meeting = Meeting::findOrFail($id);

            $data = ["state" => $status];

        $meeting->update($data);

        return redirect()->back();
    }

    public function validatePresence(Request $request, $id) {
        $meeting = Meeting::findOrFail($id);
        $user = Auth::user();
        $pivot = $user->meetings->find($id)->pivot;

        if(strcmp($request->code, $meeting->presenceCode) == 0) {
            $pivot->present = 1;
            $pivot->presentNow = 1;
            $pivot->save();
            
            //return view('show_meeting', compact('meeting', 'user', 'pivot'));

            return redirect()->back();
        } 
        
        $codeError = "Código inválido";
        return redirect()->back()->with('codeError', $codeError);
        //return view('show_meeting', compact('meeting', 'user', 'pivot', 'codeError'));
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function userIn($meetingId, $userId) {
        $meeting = Meeting::findOrFail($meetingId);
        $pivot = $meeting->users->find($userId)->pivot;
        $pivot->presentNow = 1;
        $pivot->save();

        $user = Auth::user();
        $pivot = $user->meetings->find($meetingId)->pivot;
        return redirect()->back();
        //return view('show_meeting', compact('meeting', 'user', 'pivot'));
    }
    public function userOut($meetingId, $userId) {
        $meeting = Meeting::findOrFail($meetingId);
        $pivot = $meeting->users->find($userId)->pivot;
        $pivot->presentNow = 0;
        $pivot->save();

        $user = Auth::user();
        $pivot = $user->meetings->find($meetingId)->pivot;
        return redirect()->back();
        //return view('show_meeting', compact('meeting', 'user', 'pivot'));
    }
    public function startVote($meetingId, $voteId) {
        $meeting = Meeting::findOrFail($meetingId);
        $vote = Vote::findOrFail($voteId);
        $vote->state = 1;
        $vote->votes = 0;
        $vote->save();
        foreach ($meeting->users as $key => $userC) {
            if($userC->meetings->find($meetingId)->pivot->presentNow == 1) {
                $vote->users()->save($userC);
            }
        }

        $user = Auth::user();
        $pivot = $user->meetings->find($meetingId)->pivot;
        return redirect()->back();
        //return view('show_meeting', compact('meeting', 'user', 'pivot'));
    }

    public function endVote($meetingId, $voteId) {
        $meeting = Meeting::findOrFail($meetingId);
        $vote = Vote::findOrFail($voteId);
        $vote->state = 2;
        $vote->save();

        $user = Auth::user();
        $pivot = $user->meetings->find($meetingId)->pivot;
        return redirect()->back();
        //return view('show_meeting', compact('meeting', 'user', 'pivot'));
    }

    public function vote(Request $request, $meetingId, $voteId) {
        $option = Option::findOrFail($request->option);
        $user = Auth::user();
        $vote = Vote::findOrFail($voteId);
        $pivotUserVote = $vote->users->find($user->id)->pivot;

        if($pivotUserVote->voted != 1) {
            $option->votes = $option->votes + 1;
            $option->save();
            $vote->votes = $vote->votes + 1;
            $vote->save();
            $pivotUserVote->voted = 1;
            $pivotUserVote->save();
        }

        
        $meeting = Meeting::findOrFail($meetingId);
        
        
        $pivot = $user->meetings->find($meetingId)->pivot;
        return redirect()->back();
        //return view('show_meeting', compact('meeting', 'user', 'pivot'));
    }

    /**
     * Store introduction from ata
     *
     * @param $id
     * @param Request $request
     */
    public function storeIntroduction($id, Request $request)
    {
        $meeting = Meeting::find($id);

        $rules = array(
            'text_main' => 'required',
            'filename_img' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('meetings/' . $id)
                ->withErrors($validator)
                ->withInput(Input::all());
        } else {

            $pieces = explode("/", $request->filename_img);

            $meeting->introduction = $request->text_main;
            $meeting->image = $pieces[1].'/'.$pieces[2];

            $meeting->save();
        }

        Session::flash('main', 'Introdução da ata guardada com sucesso.');
        return Redirect::to('/meetings/'.$id);

    }

    /**
     * Store introduction from ata
     *
     * @param $id
     * @param Request $request
     */
    public function storeConclusion($id, Request $request)
    {
        $meeting = Meeting::find($id);

        $rules = array(
            'conclusion' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('meetings/' . $id)
                ->withErrors($validator)
                ->withInput(Input::all());
        } else {

            $meeting->conclusion = $request->conclusion;

            $meeting->save();
        }

        Session::flash('main', 'Introdução da ata guardada com sucesso.');
        return Redirect::to('/meetings/'.$id);

    }

    /**
     * Download of main ata
     *
     * @param $id
     * @return mixed
     */
    public function downloadAta($id) {
        $meeting = Meeting::with('points')->with('organism')->find($id);

        $view = View::make("pdfs.main_ata",compact("meeting"));

        $contents = $view->render();

        $pdf = PDF::loadHTML($contents)
            ->setPaper('a4')
            ->setOption('encoding', 'utf-8')
            ->setOption('footer-font-size', 9)
            ->setOption('footer-center', 'Pág. [page] de [toPage]')
            ->setOption('margin-right', 23)
            ->setOption('margin-left', 30)
            ->setOption('margin-top', 20)
            ->setOption('margin-bottom', 30);


        return $pdf->inline("Ata_Plenario_".$meeting->organism->name.".pdf");
    }

    /**
     * Download of main ata
     *
     * @param $id
     * @return mixed
     */
    public function downloadExtractAta($id, Request $request) {
        $meeting = Meeting::with('points')->with('organism')->find($id);

        $pieces = explode('-', $request->extract_ata);


        if(count($pieces) == 2) {
            $subpoint = Subpoint::with('point')->find($pieces[1]);
            $view = View::make("pdfs.ata_extract_subpoint",compact("meeting", "subpoint"));
            $contents = $view->render();

            $pdf = PDF::loadHTML($contents)
                ->setPaper('a4')
                ->setOption('encoding', 'utf-8')
                ->setOption('footer-font-size', 9)
                ->setOption('footer-center', 'Pág. [page] de [toPage]')
                ->setOption('margin-right', 23)
                ->setOption('margin-left', 30)
                ->setOption('margin-top', 20)
                ->setOption('margin-bottom', 30);


            return $pdf->inline("Ata_Plenario_".$meeting->organism->name.".pdf");
        } else {
            $point = Point::with('subpoints')->find($pieces[0]);
            $view = View::make("pdfs.ata_extract",compact("meeting", "point"));
            $contents = $view->render();

            $pdf = PDF::loadHTML($contents)
                ->setPaper('a4')
                ->setOption('encoding', 'utf-8')
                ->setOption('footer-font-size', 9)
                ->setOption('footer-center', 'Pág. [page] de [toPage]')
                ->setOption('margin-right', 23)
                ->setOption('margin-left', 30)
                ->setOption('margin-top', 20)
                ->setOption('margin-bottom', 30);


            return $pdf->inline("Ata_Plenario_".$meeting->organism->name.".pdf");
        }

    }
}
