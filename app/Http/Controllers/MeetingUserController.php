<?php

namespace App\Http\Controllers;

use App\Meeting;
use App\MeetingUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class MeetingUserController extends Controller
{
    /**
     * Get Attendance List Pdf of a specific meeting
     *
     * @param $id
     */
    public function getAttendanceList($id) {
        $meeting = Meeting::where('id', $id)->with('organism')->first();
        $users = MeetingUser::where('meeting_id', $id)
               ->join('users','users.id','=','meeting_user.user_id')->get();

        $view = View::make("pdfs.attendance_list",compact("users", "meeting"));

        $contents = $view->render();

        $pdf = PDF::loadHTML($contents)
            ->setOption('encoding', 'utf-8')
            ->setOption('footer-font-size', 9)
            ->setOption('footer-left', utf8_decode('* Justificação de Falta'))
            ->setOption('footer-center', '[page]/[toPage]');

        return $pdf->inline("Lista_de_presenças.pdf");

    }
}
