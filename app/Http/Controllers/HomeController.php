<?php

namespace App\Http\Controllers;

use App\Meeting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $events = [];
        $data = $user->meetings;
        if($data->count()) {
            foreach ($data as $d) {
                if($d->state == 2) {
                    $color = '#008000';
                } elseif ($d->state == 1) {
                    $color = "#DC143C";
                } else {
                    $color = "#00b2b2";
                }
                $events[] = Calendar::event(
                    $d->title,
                    true,
                    $d->date,
                    $d->date,
                    $d->id,
                    [
                        'color' => $color,
                        'url' => '/meetings/'.$d->id,
                    ]
                );
            }
        }
        $calendar = Calendar::addEvents($events)
            ->setOptions([ //set fullcalendar options
                'background-color' => '#ADD8E6',
                'lang' => 'pt',
                'editable'=> true,
            ])
            ->setCallbacks([
                'eventClick' => 'function(event) {
		           window.location.href = \'/meetings/\'+event.id;
	            }',


            ]);  //add an array with addEvents

        return view('home', compact('calendar'));
    }
}
