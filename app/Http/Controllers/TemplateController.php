<?php


namespace App\Http\Controllers;


use App\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class TemplateController extends Controller
{

    public function storeTemplate(Request $request) {
        $user = Auth::user();

        $rules = array(
            'title' => 'required',
            'conteudo' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if($user->isAdmin()) {
            if ($validator->fails()) {
                Session::flash('error', 'Erro.Ambos os campos são obrigatórios');
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput(Input::all());
            } else {
                $template = new Template();

                $template->title = $request->title;
                $template->content = $request->conteudo;

                $template->save();

                Session::flash('message', 'Template guardado com sucesso.');
                return redirect()->back();
            }
        }

    }
}