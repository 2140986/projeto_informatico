<?php

namespace App\Http\Controllers;

use App\PointTemplate;
use Illuminate\Http\Request;

class PointTemplateController extends Controller
{
    /**
     * Add template to the point
     *
     * @param $point_id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addTemplateToPoint($point_id, Request $request) {

        $pointTemplate = new PointTemplate();
        $pointTemplate->point_id = $point_id;
        $pointTemplate->template_id = $request->point_template;

        $pointTemplate->save();

        return redirect()->back();

    }
}
