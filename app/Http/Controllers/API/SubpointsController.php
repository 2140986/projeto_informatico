<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Subpoint;

class SubpointsController extends Controller
{

    /**
     * Return Subpoint description
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubpointDescription($id) {
        $subpoint = Subpoint::find($id);

        return response()->json($subpoint->description);
    }

    /**
     * Return Subpoint name
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubpointName($id) {
        $subpoint = Subpoint::find($id);

        return response()->json($subpoint->name);
    }
}