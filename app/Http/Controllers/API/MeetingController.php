<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Meeting;

class MeetingController extends Controller
{
    /**
     * Return Meeting from api
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMeeting($id) {
        $meeting = Meeting::where('id', $id)->get();

        return response()->json($meeting);
    }

    /**
     * Return introduction from meeting
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getIntroductionMeeting($id) {
        $meeting = Meeting::find($id);

        return response()->json($meeting->introduction);
    }

    /**
     * Return introduction from meeting
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConclusionMeeting($id) {
        $meeting = Meeting::find($id);

        return response()->json($meeting->conclusion);
    }

}