<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Point;

class PointsController extends Controller
{
    /**
     * Return Points from meeting api
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPointsOfMeeting($id) {
        $points = Point::where('meeting_id', $id)->select('name', 'number')->get();

        return $points;
    }

    /**
     * Return Point description
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPointsDescription($id) {
        $point = Point::find($id);

        return response()->json($point->description);
    }
}