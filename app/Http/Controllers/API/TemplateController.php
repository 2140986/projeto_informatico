<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Template;

class TemplateController extends Controller
{
    /**
     * Return Template Content api
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTemplateContent($id) {
        $template = Template::find($id);

        return response()->json($template->content);
    }
}