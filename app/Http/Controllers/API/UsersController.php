<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    /**
     * Return Auth user api
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function getAuthUser() {
        return response()->json(Auth::user());
    }

}