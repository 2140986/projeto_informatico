<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMeetingPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'type' => 'required',
            'meeting_date' => 'required',
            'meeting_start' => 'required',
            'meeting_end' => 'required|max:10',
            'meeting_classroom' => 'required',
            'text' => 'required',
        ];

        foreach($this->get('rows') as $key => $val)
        {
            $rules['rows.'.$key.'.number'] = 'required|min:1';
        }

        return $rules;
    }
}
