<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'meetings';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['seqNumber', 'date', 'initDate', 'endDate', 'state', 'type',
                           'introduction', 'conclusion', 'attachments', 'minute', 'description', 'classroom', 'presenceCode'];



    public function typeToStr() {
        switch ($this->type) {
            case 0:
                return 'Ordinária';
            case 1:
                return 'Extraordinária';
        }
    }
    public function stateToStr() {
        switch ($this->state) {
            case 0:
                return 'Agendada';
            case 1:
                return 'A decorrer';
            case 2:
                return 'Finalizada';
        }
    }

    public function dateToStr() {
        $date = $this->date;
        return $date->format('d-m-Y');
    }

    public function initHourToStr() {
        return $this->initHour;
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('rejected', 'justification', 'justificationAccepted', 'role', 'present', 'presentNow');
    }

    public function organism()
    {
        return $this->belongsTo('App\Organism');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function president()
    {
        return $this->users()->with("meetings")->where('role', 1)->get()->first();
    }

    public function secretary()
    {
        return $this->users()->with("meetings")->where('role', 2)->get()->first();
    }

    public function points()
    {
        return $this->hasMany('App\Point');
    }

    public function notices()
    {
        return $this->hasMany('App\Notice');
    }

}
