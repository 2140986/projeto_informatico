<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
      /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notices';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'sendDate'];


    public function meeting()
    {
        return $this->belongsTo('App\Meeting');
    }
}
