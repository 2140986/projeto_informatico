<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
      /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'options';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'votes'];


    public function vote()
    {
        return $this->belongsTo('App\Vote');
    }
}
